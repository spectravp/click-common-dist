import { EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DragDropFilesDirective {
    fileDropped: EventEmitter<any>;
    private cssClass;
    dragOver(event: any): void;
    dragLeave(event: any): void;
    drop(event: any): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DragDropFilesDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<DragDropFilesDirective, "[clickDragDropFiles]", never, {}, { "fileDropped": "fileDropped"; }, never, never, false>;
}
//# sourceMappingURL=drag-drop-files.directive.d.ts.map