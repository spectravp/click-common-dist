import { ElementRef, EventEmitter, Renderer2 } from '@angular/core';
import * as i0 from "@angular/core";
export declare class StringifyImageDirective {
    private renderer;
    private el;
    private count;
    private internalModel;
    constructor(renderer: Renderer2, el: ElementRef);
    model: any;
    modelChange: EventEmitter<any>;
    onChange(files: any): void;
    /**
     * Sets the model value and emits the change event
     */
    private setModelValue;
    /**
     * Reads the individual file
     */
    private readFile;
    static ɵfac: i0.ɵɵFactoryDeclaration<StringifyImageDirective, never>;
    static ɵdir: i0.ɵɵDirectiveDeclaration<StringifyImageDirective, "[clickStringifyImage]", never, { "model": "model"; }, { "modelChange": "modelChange"; }, never, never, false>;
}
//# sourceMappingURL=stringify-image.directive.d.ts.map