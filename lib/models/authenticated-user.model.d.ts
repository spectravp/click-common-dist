import { ApiAuthenticationResponseModel } from './api-authentication-response.model';
import { EventEmitter } from '@angular/core';
import { AuthenticatedUserEvent } from './authenticated-user-event.model';
export declare class AuthenticatedUserModel {
    /**
     * Flag for whether data has been loaded from local storage
     */
    private static dataLoaded;
    /**
     * The id of the user record
     */
    private static id;
    /**
     * The access token of the user
     */
    private static accessToken;
    /**
     * The expiration date of the user
     */
    private static expirationDate;
    /**
     * The raw expiration time
     */
    private static expires;
    /**
     * The scope of the token
     */
    private static scope;
    /**
     * Encryption string for storage
     */
    private static encryptSecretKey;
    /**
     * Storage Event registered flag
     */
    private static storageEventListenerRegistered;
    /**
     * An event emitter so you can listen for authentication changes
     * @private
     */
    private static onChangesEventEmitter;
    /**
     * Builds the user object
     * @param apiResponse The raw api response from httpClient
     */
    constructor(apiResponse?: ApiAuthenticationResponseModel | any | null);
    /**
     * Destroys the user
     */
    destroyUser(): void;
    /**
     * Gets the users ID
     */
    get id(): string;
    /**
     * Gets the access token
     */
    get accessToken(): string;
    /**
     * Gets the expiration Date
     */
    get expirationDate(): Date;
    /**
     * Gets the scope
     */
    get scope(): string;
    /**
     * Gets the raw expires value
     */
    get expires(): number;
    /**
     * Tests if it has a valid user
     */
    isValid(): boolean;
    /**
     * Gets a copy as an object
     */
    getCopy(): {
        id: string;
        accessToken: string;
        expirationDate: string;
        scope: string;
    };
    /**
     * Gets the data as a JSON string
     */
    getJson(): string;
    /**
     * Gets the on changes event listener
     */
    get onChanges(): EventEmitter<AuthenticatedUserEvent>;
    private emitChanges;
}
//# sourceMappingURL=authenticated-user.model.d.ts.map