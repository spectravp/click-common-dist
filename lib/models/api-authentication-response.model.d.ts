export declare class ApiAuthenticationResponseModel {
    access_token: string;
    expires: number;
    scope: string;
    token_type: string;
    user_id: string;
}
//# sourceMappingURL=api-authentication-response.model.d.ts.map