import * as i0 from "@angular/core";
export declare class ClickModuleOptions {
    private static _apiUrl;
    private static _logging;
    private static _loginRoute;
    private static _apiSecretKey;
    private static _locked;
    static setOptions(options: object): void;
    static get apiUrl(): string;
    static set apiUrl(value: string);
    static get logging(): boolean;
    static set logging(value: boolean);
    static get loginRoute(): string;
    static set loginRoute(value: string);
    static get apiSecretKey(): any;
    static set apiSecretKey(value: any);
    static lock(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ClickModuleOptions, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ClickModuleOptions>;
}
//# sourceMappingURL=click-module-options.model.d.ts.map