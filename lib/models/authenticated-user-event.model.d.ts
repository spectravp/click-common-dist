import { AuthenticatedUserModel } from "./authenticated-user.model";
export declare class AuthenticatedUserEvent {
    static readonly AUTHENTICATED = "AUTHENTICATED";
    static readonly DEAUTHENTICATED = "DEAUTHENTICATED";
    type: string;
    model: AuthenticatedUserModel;
}
//# sourceMappingURL=authenticated-user-event.model.d.ts.map