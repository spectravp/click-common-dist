import { Subject } from 'rxjs';
export declare class ComponentBridgeSubjectsModel {
    id: string;
    subject: Subject<any>;
    constructor(id: string, subject: Subject<any>);
}
//# sourceMappingURL=component-bridge-subjects.model.d.ts.map