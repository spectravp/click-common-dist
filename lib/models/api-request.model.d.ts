import { HttpHeaders, HttpParams } from '@angular/common/http';
export declare class ApiRequestModel {
    url: string;
    method: string;
    body?: any;
    params?: HttpParams;
    headers?: HttpHeaders;
}
//# sourceMappingURL=api-request.model.d.ts.map