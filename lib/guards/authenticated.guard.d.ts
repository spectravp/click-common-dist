import { CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class AuthenticatedGuard implements CanActivate, CanActivateChild, CanLoad {
    private router;
    /**
     * The authenticated user model
     */
    private user;
    /**
     * Constructor
     */
    constructor(router: Router);
    /**
     * Can Activate
     */
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
    /**
     * Can Activate Child
     */
    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
    /**
     * Can Load
     */
    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean;
    /**
     * Tests if the user is valid and redirects if route is not null
     */
    private guard;
    static ɵfac: i0.ɵɵFactoryDeclaration<AuthenticatedGuard, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AuthenticatedGuard>;
}
//# sourceMappingURL=authenticated.guard.d.ts.map