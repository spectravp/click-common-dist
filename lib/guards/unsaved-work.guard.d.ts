import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AbstractFormComponent } from '../forms/abstract-form.component';
import * as i0 from "@angular/core";
export declare class UnsavedWorkGuard implements CanDeactivate<AbstractFormComponent> {
    canDeactivate(component: AbstractFormComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
    static ɵfac: i0.ɵɵFactoryDeclaration<UnsavedWorkGuard, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<UnsavedWorkGuard>;
}
//# sourceMappingURL=unsaved-work.guard.d.ts.map