import { UntypedFormGroup } from '@angular/forms';
import * as i0 from "@angular/core";
export declare abstract class AbstractFormComponent {
    /**
     * The form group object
     */
    abstract form: UntypedFormGroup;
    /**
     * Submitted flag
     */
    protected _submitted: boolean;
    /**
     * Host listener for back button protection
     */
    unloadNotification($event: any): void;
    get submitted(): boolean;
    /**
     * Whether the component can deactivate or not
     */
    canDeactivate(): boolean;
    static ɵfac: i0.ɵɵFactoryDeclaration<AbstractFormComponent, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AbstractFormComponent>;
}
//# sourceMappingURL=abstract-form.component.d.ts.map