import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { ClickModuleOptions } from '../models/click-module-options.model';
import { ApiRequestModel } from '../models/api-request.model';
import * as i0 from "@angular/core";
export declare class ApiService {
    private options;
    private httpClient;
    static GET: string;
    static POST: string;
    static PUT: string;
    static PATCH: string;
    static DELETE: string;
    /**
     * Constructor
     */
    constructor(options: ClickModuleOptions, httpClient: HttpClient);
    /**
     * Makes an API call
     * You can pass in individual variables, or you can use an
     * object that conforms to the ApiRequestModel interface:
     * {url: '/somethinbg'}
     * {url: '/something, method: ApiService.POST, body: {name: 'test'}}
     */
    call(url: string | ApiRequestModel | HttpRequest<any>, method?: string, body?: any, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * Executes a GET
     */
    get(url: string | ApiRequestModel | HttpRequest<any>, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * Executes a POST
     */
    post(url: string | ApiRequestModel | HttpRequest<any>, body?: any, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * POST as form data
     */
    postFormData(url: string | ApiRequestModel | HttpRequest<any>, body?: any): Promise<any>;
    /**
     * Executes a PUT
     */
    put(url: string | ApiRequestModel | HttpRequest<any>, body?: any, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * PUT as form data
     */
    putFormData(url: string | ApiRequestModel | HttpRequest<any>, body?: any): Promise<any>;
    /**
     * Executes a PATCH
     */
    patch(url: string | ApiRequestModel | HttpRequest<any>, body?: any, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * Executes a DELETE
     */
    delete(url: string | ApiRequestModel | HttpRequest<any>, params?: HttpParams, headers?: HttpHeaders): Promise<any>;
    /**
     * Logs a request
     */
    private logRequest;
    /**
     * Builds the request object
     */
    private buildRequest;
    static ɵfac: i0.ɵɵFactoryDeclaration<ApiService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ApiService>;
}
//# sourceMappingURL=api.service.d.ts.map