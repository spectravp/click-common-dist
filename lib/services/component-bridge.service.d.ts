import { Observable, Subject } from 'rxjs';
import * as i0 from "@angular/core";
export declare class ComponentBridgeService {
    /**
     * An array that stores all the subjects
     */
    private subjects;
    /**
     * Sends a message
     */
    send(id: string, message: any): void;
    /**
     * Subscribes to messages
     */
    subscribe(id: string): Observable<any>;
    /**
     * Destroys the subscription
     */
    unsubscribeAll(id: string): void;
    /**
     * Gets the transport
     */
    protected getTransport(id: string): Subject<any>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ComponentBridgeService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<ComponentBridgeService>;
}
//# sourceMappingURL=component-bridge.service.d.ts.map