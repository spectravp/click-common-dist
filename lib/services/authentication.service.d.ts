import { AuthenticatedUserModel } from '../models/authenticated-user.model';
import { ApiService } from './api.service';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
export declare class AuthenticationService {
    private apiService;
    /**
     * The authenticated user data
     */
    private authenticatedUser;
    /**
     * Constructor
     */
    constructor(apiService: ApiService);
    /**
     * Authenticates a user
     * Returns a promise so you can use await
     */
    authenticate(data: any, url?: string, params?: HttpParams, headers?: HttpHeaders): Promise<AuthenticatedUserModel>;
    /**
     * Destroys a user
     */
    destroy(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<AuthenticationService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AuthenticationService>;
}
//# sourceMappingURL=authentication.service.d.ts.map