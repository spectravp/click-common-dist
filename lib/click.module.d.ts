import { ModuleWithProviders, InjectionToken } from '@angular/core';
import { ClickModuleOptions } from './models/click-module-options.model';
import { ApiService } from './services/api.service';
import { Hash } from './utils/hash';
import * as i0 from "@angular/core";
import * as i1 from "./components/api-error/api-error.component";
import * as i2 from "./directives/stringify-image.directive";
import * as i3 from "./pipes/mongo-date.pipe";
import * as i4 from "./directives/drag-drop-files.directive";
import * as i5 from "@angular/common";
import * as i6 from "@angular/common/http";
export { ClickModuleOptions };
export { ApiService };
export { Hash };
export declare class ClickModule {
    static forRoot(options?: ModuleOptions): ModuleWithProviders<ClickModule>;
    static ɵfac: i0.ɵɵFactoryDeclaration<ClickModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<ClickModule, [typeof i1.ApiErrorComponent, typeof i2.StringifyImageDirective, typeof i3.MongoDatePipe, typeof i4.DragDropFilesDirective], [typeof i5.CommonModule, typeof i6.HttpClientModule], [typeof i1.ApiErrorComponent, typeof i2.StringifyImageDirective, typeof i3.MongoDatePipe, typeof i4.DragDropFilesDirective]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<ClickModule>;
}
export interface ModuleOptions {
    apiUrl?: string;
    logging?: boolean;
    loginRoute?: string;
    apiSecretKey?: string;
}
export declare const FOR_ROOT_OPTIONS_TOKEN: InjectionToken<ModuleOptions>;
export declare function provideApiServiceOptions(options?: ModuleOptions): ClickModuleOptions;
//# sourceMappingURL=click.module.d.ts.map