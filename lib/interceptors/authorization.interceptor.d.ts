import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class AuthorizationInterceptor implements HttpInterceptor {
    /**
     * Constructor
     */
    constructor();
    /**
     * Interceptor to add the Authorization header if we have a valid user
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    static ɵfac: i0.ɵɵFactoryDeclaration<AuthorizationInterceptor, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<AuthorizationInterceptor>;
}
//# sourceMappingURL=authorization.interceptor.d.ts.map