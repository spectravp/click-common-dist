import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class MongoDatePipe implements PipeTransform {
    transform(value: any): Date | string | number | null;
    static ɵfac: i0.ɵɵFactoryDeclaration<MongoDatePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<MongoDatePipe, "mongoDate", false>;
}
//# sourceMappingURL=mongo-date.pipe.d.ts.map