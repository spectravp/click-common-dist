import { OnChanges, SimpleChanges } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as i0 from "@angular/core";
export declare class ApiErrorComponent implements OnChanges {
    apiErrors: HttpErrorResponse;
    key: string;
    prependAsterisk: boolean;
    errors: string[];
    constructor();
    ngOnChanges(changes: SimpleChanges): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<ApiErrorComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<ApiErrorComponent, "click-api-error", never, { "apiErrors": "apiErrors"; "key": "key"; "prependAsterisk": "prependAsterisk"; }, {}, never, never, false>;
}
//# sourceMappingURL=api-error.component.d.ts.map