import { Injectable } from '@angular/core';
import { ComponentBridgeSubjectsModel } from '../models/component-bridge-subjects.model';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
export class ComponentBridgeService {
    constructor() {
        /**
         * An array that stores all the subjects
         */
        this.subjects = [];
    }
    /**
     * Sends a message
     */
    send(id, message) {
        this.getTransport(id).next(message);
    }
    /**
     * Subscribes to messages
     */
    subscribe(id) {
        return this.getTransport(id).asObservable();
    }
    /**
     * Destroys the subscription
     */
    unsubscribeAll(id) {
        for (let i = 0; i < this.subjects.length; i++) {
            if (this.subjects[i].id === id) {
                this.subjects.splice(i, 1);
                break;
            }
        }
    }
    /**
     * Gets the transport
     */
    getTransport(id) {
        for (let i = 0; i < this.subjects.length; i++) {
            if (this.subjects[i].id === id) {
                return this.subjects[i].subject;
            }
        }
        // if we get here, we dont have one, auto-create it
        const transport = new ComponentBridgeSubjectsModel(id, new Subject());
        this.subjects.push(transport);
        return transport.subject;
    }
}
ComponentBridgeService.ɵfac = function ComponentBridgeService_Factory(t) { return new (t || ComponentBridgeService)(); };
ComponentBridgeService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ComponentBridgeService, factory: ComponentBridgeService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ComponentBridgeService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50LWJyaWRnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY2xpY2svc3JjL2xpYi9zZXJ2aWNlcy9jb21wb25lbnQtYnJpZGdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSwyQ0FBMkMsQ0FBQztBQUN2RixPQUFPLEVBQWEsT0FBTyxFQUFDLE1BQU0sTUFBTSxDQUFDOztBQUt6QyxNQUFNLE9BQU8sc0JBQXNCO0lBSG5DO1FBS0U7O1dBRUc7UUFDSyxhQUFRLEdBQW1DLEVBQUUsQ0FBQztLQTJDdkQ7SUF6Q0M7O09BRUc7SUFDSSxJQUFJLENBQUMsRUFBVSxFQUFFLE9BQVk7UUFDbEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVEOztPQUVHO0lBQ0ksU0FBUyxDQUFDLEVBQVU7UUFDekIsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzlDLENBQUM7SUFFRDs7T0FFRztJQUNJLGNBQWMsQ0FBQyxFQUFVO1FBQzlCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixNQUFNO2FBQ1A7U0FDRjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNPLFlBQVksQ0FBQyxFQUFVO1FBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM3QyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDOUIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQzthQUNqQztTQUNGO1FBRUQsbURBQW1EO1FBQ25ELE1BQU0sU0FBUyxHQUFHLElBQUksNEJBQTRCLENBQUMsRUFBRSxFQUFFLElBQUksT0FBTyxFQUFPLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QixPQUFPLFNBQVMsQ0FBQyxPQUFPLENBQUM7SUFDM0IsQ0FBQzs7NEZBL0NVLHNCQUFzQjs0RUFBdEIsc0JBQXNCLFdBQXRCLHNCQUFzQixtQkFGckIsTUFBTTt1RkFFUCxzQkFBc0I7Y0FIbEMsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtDb21wb25lbnRCcmlkZ2VTdWJqZWN0c01vZGVsfSBmcm9tICcuLi9tb2RlbHMvY29tcG9uZW50LWJyaWRnZS1zdWJqZWN0cy5tb2RlbCc7XG5pbXBvcnQge09ic2VydmFibGUsIFN1YmplY3R9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21wb25lbnRCcmlkZ2VTZXJ2aWNlIHtcblxuICAvKipcbiAgICogQW4gYXJyYXkgdGhhdCBzdG9yZXMgYWxsIHRoZSBzdWJqZWN0c1xuICAgKi9cbiAgcHJpdmF0ZSBzdWJqZWN0czogQ29tcG9uZW50QnJpZGdlU3ViamVjdHNNb2RlbFtdID0gW107XG5cbiAgLyoqXG4gICAqIFNlbmRzIGEgbWVzc2FnZVxuICAgKi9cbiAgcHVibGljIHNlbmQoaWQ6IHN0cmluZywgbWVzc2FnZTogYW55KTogdm9pZCB7XG4gICAgdGhpcy5nZXRUcmFuc3BvcnQoaWQpLm5leHQobWVzc2FnZSk7XG4gIH1cblxuICAvKipcbiAgICogU3Vic2NyaWJlcyB0byBtZXNzYWdlc1xuICAgKi9cbiAgcHVibGljIHN1YnNjcmliZShpZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5nZXRUcmFuc3BvcnQoaWQpLmFzT2JzZXJ2YWJsZSgpO1xuICB9XG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIHRoZSBzdWJzY3JpcHRpb25cbiAgICovXG4gIHB1YmxpYyB1bnN1YnNjcmliZUFsbChpZDogc3RyaW5nKTogdm9pZCB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnN1YmplY3RzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAodGhpcy5zdWJqZWN0c1tpXS5pZCA9PT0gaWQpIHtcbiAgICAgICAgdGhpcy5zdWJqZWN0cy5zcGxpY2UoaSwgMSk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSB0cmFuc3BvcnRcbiAgICovXG4gIHByb3RlY3RlZCBnZXRUcmFuc3BvcnQoaWQ6IHN0cmluZyk6IFN1YmplY3Q8YW55PiB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnN1YmplY3RzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAodGhpcy5zdWJqZWN0c1tpXS5pZCA9PT0gaWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3ViamVjdHNbaV0uc3ViamVjdDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBpZiB3ZSBnZXQgaGVyZSwgd2UgZG9udCBoYXZlIG9uZSwgYXV0by1jcmVhdGUgaXRcbiAgICBjb25zdCB0cmFuc3BvcnQgPSBuZXcgQ29tcG9uZW50QnJpZGdlU3ViamVjdHNNb2RlbChpZCwgbmV3IFN1YmplY3Q8YW55PigpKTtcbiAgICB0aGlzLnN1YmplY3RzLnB1c2godHJhbnNwb3J0KTtcbiAgICByZXR1cm4gdHJhbnNwb3J0LnN1YmplY3Q7XG4gIH1cbn1cbiJdfQ==