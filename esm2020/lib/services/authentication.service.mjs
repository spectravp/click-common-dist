import { Injectable } from '@angular/core';
import { AuthenticatedUserModel } from '../models/authenticated-user.model';
import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "./api.service";
export class AuthenticationService {
    /**
     * Constructor
     */
    constructor(apiService) {
        this.apiService = apiService;
        /**
         * The authenticated user data
         */
        this.authenticatedUser = null;
    }
    /**
     * Authenticates a user
     * Returns a promise so you can use await
     */
    authenticate(data, url = null, params = null, headers = null) {
        // always destroy the user before remaking it
        this.destroy();
        if (url === null) {
            url = 'admin/authenticate';
        }
        return new Promise((resolve, reject) => {
            let base = this.apiService.post(url, data, params, headers);
            if (base instanceof Observable) { // make sure we are always a promise
                base = base.toPromise();
            }
            base.then(response => {
                resolve(new AuthenticatedUserModel(response));
            }).catch(error => {
                reject(error);
            });
        });
    }
    /**
     * Destroys a user
     */
    destroy() {
        if (this.authenticatedUser === null) {
            this.authenticatedUser = new AuthenticatedUserModel();
        }
        this.authenticatedUser.destroyUser();
    }
}
AuthenticationService.ɵfac = function AuthenticationService_Factory(t) { return new (t || AuthenticationService)(i0.ɵɵinject(i1.ApiService)); };
AuthenticationService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthenticationService, factory: AuthenticationService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthenticationService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.ApiService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvc2VydmljZXMvYXV0aGVudGljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLG9DQUFvQyxDQUFDO0FBRTFFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxNQUFNLENBQUM7OztBQU1oQyxNQUFNLE9BQU8scUJBQXFCO0lBT2hDOztPQUVHO0lBQ0gsWUFBb0IsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQVIxQzs7V0FFRztRQUNLLHNCQUFpQixHQUEyQixJQUFJLENBQUM7SUFLWixDQUFDO0lBRTlDOzs7T0FHRztJQUNILFlBQVksQ0FBQyxJQUFTLEVBQUUsTUFBYyxJQUFJLEVBQUUsU0FBcUIsSUFBSSxFQUFFLFVBQXVCLElBQUk7UUFDaEcsNkNBQTZDO1FBQzdDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVmLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtZQUNoQixHQUFHLEdBQUcsb0JBQW9CLENBQUM7U0FDNUI7UUFFRCxPQUFPLElBQUksT0FBTyxDQUF5QixDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUM3RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM1RCxJQUFJLElBQUksWUFBWSxVQUFVLEVBQUUsRUFBRSxvQ0FBb0M7Z0JBQ3BFLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7YUFDekI7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNuQixPQUFPLENBQUMsSUFBSSxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDZixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDaEIsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7T0FFRztJQUNILE9BQU87UUFDTCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsS0FBSyxJQUFJLEVBQUc7WUFDcEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksc0JBQXNCLEVBQUUsQ0FBQztTQUN2RDtRQUVELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QyxDQUFDOzswRkE5Q1UscUJBQXFCOzJFQUFyQixxQkFBcUIsV0FBckIscUJBQXFCLG1CQUZwQixNQUFNO3VGQUVQLHFCQUFxQjtjQUhqQyxVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0F1dGhlbnRpY2F0ZWRVc2VyTW9kZWx9IGZyb20gJy4uL21vZGVscy9hdXRoZW50aWNhdGVkLXVzZXIubW9kZWwnO1xuaW1wb3J0IHtBcGlTZXJ2aWNlfSBmcm9tICcuL2FwaS5zZXJ2aWNlJztcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0h0dHBIZWFkZXJzLCBIdHRwUGFyYW1zfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uU2VydmljZSB7XG5cbiAgLyoqXG4gICAqIFRoZSBhdXRoZW50aWNhdGVkIHVzZXIgZGF0YVxuICAgKi9cbiAgcHJpdmF0ZSBhdXRoZW50aWNhdGVkVXNlcjogQXV0aGVudGljYXRlZFVzZXJNb2RlbCA9IG51bGw7XG5cbiAgLyoqXG4gICAqIENvbnN0cnVjdG9yXG4gICAqL1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFwaVNlcnZpY2UpIHt9XG5cbiAgLyoqXG4gICAqIEF1dGhlbnRpY2F0ZXMgYSB1c2VyXG4gICAqIFJldHVybnMgYSBwcm9taXNlIHNvIHlvdSBjYW4gdXNlIGF3YWl0XG4gICAqL1xuICBhdXRoZW50aWNhdGUoZGF0YTogYW55LCB1cmw6IHN0cmluZyA9IG51bGwsIHBhcmFtczogSHR0cFBhcmFtcyA9IG51bGwsIGhlYWRlcnM6IEh0dHBIZWFkZXJzID0gbnVsbCk6IFByb21pc2U8QXV0aGVudGljYXRlZFVzZXJNb2RlbD4ge1xuICAgIC8vIGFsd2F5cyBkZXN0cm95IHRoZSB1c2VyIGJlZm9yZSByZW1ha2luZyBpdFxuICAgIHRoaXMuZGVzdHJveSgpO1xuXG4gICAgaWYgKHVybCA9PT0gbnVsbCkge1xuICAgICAgdXJsID0gJ2FkbWluL2F1dGhlbnRpY2F0ZSc7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWw+KChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBiYXNlID0gdGhpcy5hcGlTZXJ2aWNlLnBvc3QodXJsLCBkYXRhLCBwYXJhbXMsIGhlYWRlcnMpO1xuICAgICAgaWYgKGJhc2UgaW5zdGFuY2VvZiBPYnNlcnZhYmxlKSB7IC8vIG1ha2Ugc3VyZSB3ZSBhcmUgYWx3YXlzIGEgcHJvbWlzZVxuICAgICAgICBiYXNlID0gYmFzZS50b1Byb21pc2UoKTtcbiAgICAgIH1cbiAgICAgIGJhc2UudGhlbihyZXNwb25zZSA9PiB7XG4gICAgICAgIHJlc29sdmUobmV3IEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwocmVzcG9uc2UpKTtcbiAgICAgIH0pLmNhdGNoKGVycm9yID0+IHtcbiAgICAgICAgcmVqZWN0KGVycm9yKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIGEgdXNlclxuICAgKi9cbiAgZGVzdHJveSgpIHtcbiAgICBpZiAodGhpcy5hdXRoZW50aWNhdGVkVXNlciA9PT0gbnVsbCApIHtcbiAgICAgIHRoaXMuYXV0aGVudGljYXRlZFVzZXIgPSBuZXcgQXV0aGVudGljYXRlZFVzZXJNb2RlbCgpO1xuICAgIH1cblxuICAgIHRoaXMuYXV0aGVudGljYXRlZFVzZXIuZGVzdHJveVVzZXIoKTtcbiAgfVxufVxuIl19