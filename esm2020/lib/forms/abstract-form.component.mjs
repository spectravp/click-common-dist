import { HostListener, Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class AbstractFormComponent {
    constructor() {
        /**
         * Submitted flag
         */
        this._submitted = false;
    }
    /**
     * Host listener for back button protection
     */
    unloadNotification($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = true;
        }
    }
    get submitted() {
        return this._submitted;
    }
    /**
     * Whether the component can deactivate or not
     */
    canDeactivate() {
        return this._submitted || !this.form.dirty;
    }
}
AbstractFormComponent.ɵfac = function AbstractFormComponent_Factory(t) { return new (t || AbstractFormComponent)(); };
AbstractFormComponent.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AbstractFormComponent, factory: AbstractFormComponent.ɵfac });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AbstractFormComponent, [{
        type: Injectable
    }], null, { unloadNotification: [{
            type: HostListener,
            args: ['window:beforeunload', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJzdHJhY3QtZm9ybS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jbGljay9zcmMvbGliL2Zvcm1zL2Fic3RyYWN0LWZvcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxZQUFZLEVBQUUsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDOztBQUl2RCxNQUFNLE9BQWdCLHFCQUFxQjtJQUQzQztRQVFFOztXQUVHO1FBQ08sZUFBVSxHQUFHLEtBQUssQ0FBQztLQXVCOUI7SUFyQkM7O09BRUc7SUFFSCxrQkFBa0IsQ0FBQyxNQUFXO1FBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDekIsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7U0FDM0I7SUFDSCxDQUFDO0lBR0QsSUFBSSxTQUFTO1FBQ1gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7T0FFRztJQUNILGFBQWE7UUFDWCxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUM3QyxDQUFDOzswRkFoQ21CLHFCQUFxQjsyRUFBckIscUJBQXFCLFdBQXJCLHFCQUFxQjt1RkFBckIscUJBQXFCO2NBRDFDLFVBQVU7Z0JBaUJULGtCQUFrQjtrQkFEakIsWUFBWTttQkFBQyxxQkFBcUIsRUFBRSxDQUFDLFFBQVEsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SG9zdExpc3RlbmVyLCBJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7VW50eXBlZEZvcm1Hcm91cH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQWJzdHJhY3RGb3JtQ29tcG9uZW50IHtcblxuICAvKipcbiAgICogVGhlIGZvcm0gZ3JvdXAgb2JqZWN0XG4gICAqL1xuICBhYnN0cmFjdCBmb3JtOiBVbnR5cGVkRm9ybUdyb3VwO1xuXG4gIC8qKlxuICAgKiBTdWJtaXR0ZWQgZmxhZ1xuICAgKi9cbiAgcHJvdGVjdGVkIF9zdWJtaXR0ZWQgPSBmYWxzZTtcblxuICAvKipcbiAgICogSG9zdCBsaXN0ZW5lciBmb3IgYmFjayBidXR0b24gcHJvdGVjdGlvblxuICAgKi9cbiAgQEhvc3RMaXN0ZW5lcignd2luZG93OmJlZm9yZXVubG9hZCcsIFsnJGV2ZW50J10pXG4gIHVubG9hZE5vdGlmaWNhdGlvbigkZXZlbnQ6IGFueSkge1xuICAgIGlmICghdGhpcy5jYW5EZWFjdGl2YXRlKCkpIHtcbiAgICAgICRldmVudC5yZXR1cm5WYWx1ZSA9IHRydWU7XG4gICAgfVxuICB9XG5cblxuICBnZXQgc3VibWl0dGVkKCk6IGJvb2xlYW4ge1xuICAgIHJldHVybiB0aGlzLl9zdWJtaXR0ZWQ7XG4gIH1cblxuICAvKipcbiAgICogV2hldGhlciB0aGUgY29tcG9uZW50IGNhbiBkZWFjdGl2YXRlIG9yIG5vdFxuICAgKi9cbiAgY2FuRGVhY3RpdmF0ZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5fc3VibWl0dGVkIHx8ICF0aGlzLmZvcm0uZGlydHk7XG4gIH1cbn1cbiJdfQ==