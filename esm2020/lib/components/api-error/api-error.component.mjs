import { Component, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function ApiErrorComponent_ul_0_li_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "li");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const error_r2 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(error_r2);
} }
function ApiErrorComponent_ul_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "ul", 1);
    i0.ɵɵtemplate(1, ApiErrorComponent_ul_0_li_1_Template, 2, 1, "li", 2);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", ctx_r0.errors);
} }
export class ApiErrorComponent {
    constructor() {
        this.prependAsterisk = false;
        this.errors = [];
    }
    ngOnChanges(changes) {
        this.errors.length = 0;
        if (!this.key) {
            console.error('No key set for API Error components');
            return;
        }
        if (this.apiErrors instanceof HttpErrorResponse && this.apiErrors.error && this.apiErrors.error.validation_messages) {
            if (this.apiErrors.error.validation_messages[this.key]) {
                for (const error in this.apiErrors.error.validation_messages[this.key]) {
                    let message = this.apiErrors.error.validation_messages[this.key][error];
                    if (this.prependAsterisk) {
                        message = '* ' + message;
                    }
                    this.errors.push(message);
                }
            }
        }
    }
}
ApiErrorComponent.ɵfac = function ApiErrorComponent_Factory(t) { return new (t || ApiErrorComponent)(); };
ApiErrorComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ApiErrorComponent, selectors: [["click-api-error"]], inputs: { apiErrors: "apiErrors", key: "key", prependAsterisk: "prependAsterisk" }, features: [i0.ɵɵNgOnChangesFeature], decls: 1, vars: 1, consts: [["class", "click-api-errors", 4, "ngIf"], [1, "click-api-errors"], [4, "ngFor", "ngForOf"]], template: function ApiErrorComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, ApiErrorComponent_ul_0_Template, 2, 1, "ul", 0);
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.errors.length);
    } }, dependencies: [i1.NgForOf, i1.NgIf], styles: ["[_nghost-%COMP%]   ul[_ngcontent-%COMP%]{margin:0;padding:0;list-style:none;display:block}[_nghost-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{color:#8b0000;font-size:.9rem;display:block}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ApiErrorComponent, [{
        type: Component,
        args: [{ selector: 'click-api-error', template: "<ul *ngIf=\"errors.length\" class=\"click-api-errors\">\n  <li *ngFor=\"let error of errors\">{{error}}</li>\n</ul>\n", styles: [":host ul{margin:0;padding:0;list-style:none;display:block}:host ul li{color:#8b0000;font-size:.9rem;display:block}\n"] }]
    }], function () { return []; }, { apiErrors: [{
            type: Input
        }], key: [{
            type: Input
        }], prependAsterisk: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLWVycm9yLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvY29tcG9uZW50cy9hcGktZXJyb3IvYXBpLWVycm9yLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvY29tcG9uZW50cy9hcGktZXJyb3IvYXBpLWVycm9yLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUEyQixNQUFNLGVBQWUsQ0FBQztBQUN6RSxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQzs7OztJQ0FyRCwwQkFBaUM7SUFBQSxZQUFTO0lBQUEsaUJBQUs7OztJQUFkLGVBQVM7SUFBVCw4QkFBUzs7O0lBRDVDLDZCQUFtRDtJQUNqRCxxRUFBK0M7SUFDakQsaUJBQUs7OztJQURtQixlQUFTO0lBQVQsdUNBQVM7O0FET2pDLE1BQU0sT0FBTyxpQkFBaUI7SUFRNUI7UUFKUyxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUUxQixXQUFNLEdBQWEsRUFBRSxDQUFDO0lBRWIsQ0FBQztJQUVqQixXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBRXZCLElBQUksQ0FBRSxJQUFJLENBQUMsR0FBRyxFQUFHO1lBQ2YsT0FBTyxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO1lBQ3JELE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsWUFBWSxpQkFBaUIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsRUFBRTtZQUNuSCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRztnQkFDdkQsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ3RFLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEUsSUFBSyxJQUFJLENBQUMsZUFBZSxFQUFHO3dCQUMxQixPQUFPLEdBQUcsSUFBSSxHQUFHLE9BQU8sQ0FBQztxQkFDMUI7b0JBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQzNCO2FBQ0Y7U0FDRjtJQUNILENBQUM7O2tGQTdCVSxpQkFBaUI7b0VBQWpCLGlCQUFpQjtRQ1I5QixnRUFFSzs7UUFGQSx3Q0FBbUI7O3VGRFFYLGlCQUFpQjtjQUw3QixTQUFTOzJCQUNFLGlCQUFpQjtzQ0FNbEIsU0FBUztrQkFBakIsS0FBSztZQUNHLEdBQUc7a0JBQVgsS0FBSztZQUNHLGVBQWU7a0JBQXZCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlc30gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBFcnJvclJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NsaWNrLWFwaS1lcnJvcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9hcGktZXJyb3IuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9hcGktZXJyb3IuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBBcGlFcnJvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG5cbiAgQElucHV0KCkgYXBpRXJyb3JzOiBIdHRwRXJyb3JSZXNwb25zZTtcbiAgQElucHV0KCkga2V5OiBzdHJpbmc7XG4gIEBJbnB1dCgpIHByZXBlbmRBc3RlcmlzayA9IGZhbHNlO1xuXG4gIHB1YmxpYyBlcnJvcnM6IHN0cmluZ1tdID0gW107XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgdGhpcy5lcnJvcnMubGVuZ3RoID0gMDtcblxuICAgIGlmICghIHRoaXMua2V5ICkge1xuICAgICAgY29uc29sZS5lcnJvcignTm8ga2V5IHNldCBmb3IgQVBJIEVycm9yIGNvbXBvbmVudHMnKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5hcGlFcnJvcnMgaW5zdGFuY2VvZiBIdHRwRXJyb3JSZXNwb25zZSAmJiB0aGlzLmFwaUVycm9ycy5lcnJvciAmJiB0aGlzLmFwaUVycm9ycy5lcnJvci52YWxpZGF0aW9uX21lc3NhZ2VzKSB7XG4gICAgICBpZiAodGhpcy5hcGlFcnJvcnMuZXJyb3IudmFsaWRhdGlvbl9tZXNzYWdlc1t0aGlzLmtleV0gKSB7XG4gICAgICAgIGZvciAoY29uc3QgZXJyb3IgaW4gdGhpcy5hcGlFcnJvcnMuZXJyb3IudmFsaWRhdGlvbl9tZXNzYWdlc1t0aGlzLmtleV0pIHtcbiAgICAgICAgICBsZXQgbWVzc2FnZSA9IHRoaXMuYXBpRXJyb3JzLmVycm9yLnZhbGlkYXRpb25fbWVzc2FnZXNbdGhpcy5rZXldW2Vycm9yXTtcbiAgICAgICAgICBpZiAoIHRoaXMucHJlcGVuZEFzdGVyaXNrICkge1xuICAgICAgICAgICAgbWVzc2FnZSA9ICcqICcgKyBtZXNzYWdlO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLmVycm9ycy5wdXNoKG1lc3NhZ2UpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iLCI8dWwgKm5nSWY9XCJlcnJvcnMubGVuZ3RoXCIgY2xhc3M9XCJjbGljay1hcGktZXJyb3JzXCI+XG4gIDxsaSAqbmdGb3I9XCJsZXQgZXJyb3Igb2YgZXJyb3JzXCI+e3tlcnJvcn19PC9saT5cbjwvdWw+XG4iXX0=