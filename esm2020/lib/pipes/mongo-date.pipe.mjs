import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class MongoDatePipe {
    transform(value) {
        if (value instanceof Date) {
            return value;
        }
        if (value?.$date?.$numberLong) {
            // @ts-ignore
            return new Date(parseInt(value?.$date?.$numberLong));
        }
        return value;
    }
}
MongoDatePipe.ɵfac = function MongoDatePipe_Factory(t) { return new (t || MongoDatePipe)(); };
MongoDatePipe.ɵpipe = /*@__PURE__*/ i0.ɵɵdefinePipe({ name: "mongoDate", type: MongoDatePipe, pure: true });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MongoDatePipe, [{
        type: Pipe,
        args: [{
                name: 'mongoDate'
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9uZ28tZGF0ZS5waXBlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY2xpY2svc3JjL2xpYi9waXBlcy9tb25nby1kYXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBS3BELE1BQU0sT0FBTyxhQUFhO0lBRXhCLFNBQVMsQ0FBQyxLQUFVO1FBQ2xCLElBQUksS0FBSyxZQUFZLElBQUksRUFBRTtZQUN6QixPQUFPLEtBQUssQ0FBQztTQUNkO1FBRUQsSUFBSSxLQUFLLEVBQUUsS0FBSyxFQUFFLFdBQVcsRUFBRTtZQUM3QixhQUFhO1lBQ2IsT0FBTyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQ3REO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzswRUFiVSxhQUFhOytFQUFiLGFBQWE7dUZBQWIsYUFBYTtjQUh6QixJQUFJO2VBQUM7Z0JBQ0osSUFBSSxFQUFFLFdBQVc7YUFDbEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBQaXBlKHtcbiAgbmFtZTogJ21vbmdvRGF0ZSdcbn0pXG5leHBvcnQgY2xhc3MgTW9uZ29EYXRlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gIHRyYW5zZm9ybSh2YWx1ZTogYW55KTogRGF0ZXxzdHJpbmd8bnVtYmVyfG51bGwge1xuICAgIGlmICh2YWx1ZSBpbnN0YW5jZW9mIERhdGUpIHtcbiAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG5cbiAgICBpZiAodmFsdWU/LiRkYXRlPy4kbnVtYmVyTG9uZykge1xuICAgICAgLy8gQHRzLWlnbm9yZVxuICAgICAgcmV0dXJuIG5ldyBEYXRlKHBhcnNlSW50KHZhbHVlPy4kZGF0ZT8uJG51bWJlckxvbmcpKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdmFsdWU7XG4gIH1cbn1cbiJdfQ==