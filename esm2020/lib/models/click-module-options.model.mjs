import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class ClickModuleOptions {
    static setOptions(options) {
        if (options) {
            for (const prop in options) {
                ClickModuleOptions[prop] = options[prop];
            }
        }
    }
    static get apiUrl() {
        return this._apiUrl;
    }
    static set apiUrl(value) {
        if (!ClickModuleOptions._locked) {
            this._apiUrl = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get logging() {
        return this._logging;
    }
    static set logging(value) {
        if (!ClickModuleOptions._locked) {
            this._logging = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get loginRoute() {
        return this._loginRoute;
    }
    static set loginRoute(value) {
        if (!ClickModuleOptions._locked) {
            this._loginRoute = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get apiSecretKey() {
        return this._apiSecretKey;
    }
    static set apiSecretKey(value) {
        if (!ClickModuleOptions._locked) {
            this._apiSecretKey = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static lock() {
        ClickModuleOptions._locked = true;
    }
}
ClickModuleOptions._apiUrl = '/api/';
ClickModuleOptions._logging = true;
ClickModuleOptions._loginRoute = '/login';
ClickModuleOptions._locked = false;
ClickModuleOptions.ɵfac = function ClickModuleOptions_Factory(t) { return new (t || ClickModuleOptions)(); };
ClickModuleOptions.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ClickModuleOptions, factory: ClickModuleOptions.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ClickModuleOptions, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2stbW9kdWxlLW9wdGlvbnMubW9kZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9jbGljay9zcmMvbGliL21vZGVscy9jbGljay1tb2R1bGUtb3B0aW9ucy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszQyxNQUFNLE9BQU8sa0JBQWtCO0lBTzdCLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBZTtRQUMvQixJQUFJLE9BQU8sRUFBRTtZQUNYLEtBQUssTUFBTSxJQUFJLElBQUksT0FBTyxFQUFFO2dCQUMxQixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDMUM7U0FDRjtJQUNILENBQUM7SUFFRCxNQUFNLEtBQUssTUFBTTtRQUNmLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN0QixDQUFDO0lBRUQsTUFBTSxLQUFLLE1BQU0sQ0FBQyxLQUFhO1FBQzdCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7WUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDdEI7YUFBTTtZQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsbUZBQW1GLENBQUMsQ0FBQztTQUN0RztJQUNILENBQUM7SUFFRCxNQUFNLEtBQUssT0FBTztRQUNoQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQztJQUVELE1BQU0sS0FBSyxPQUFPLENBQUMsS0FBYztRQUMvQixJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFO1lBQy9CLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1NBQ3ZCO2FBQU07WUFDTCxNQUFNLElBQUksS0FBSyxDQUFDLG1GQUFtRixDQUFDLENBQUM7U0FDdEc7SUFDSCxDQUFDO0lBRUQsTUFBTSxLQUFLLFVBQVU7UUFDbkIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzFCLENBQUM7SUFFRCxNQUFNLEtBQUssVUFBVSxDQUFDLEtBQWE7UUFDakMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtZQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztTQUMxQjthQUFNO1lBQ0wsTUFBTSxJQUFJLEtBQUssQ0FBQyxtRkFBbUYsQ0FBQyxDQUFDO1NBQ3RHO0lBQ0gsQ0FBQztJQUVELE1BQU0sS0FBSyxZQUFZO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0lBRUQsTUFBTSxLQUFLLFlBQVksQ0FBQyxLQUFLO1FBQzNCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7WUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7U0FDNUI7YUFBTTtZQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsbUZBQW1GLENBQUMsQ0FBQztTQUN0RztJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsSUFBSTtRQUNULGtCQUFrQixDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDcEMsQ0FBQzs7QUFoRWMsMEJBQU8sR0FBRyxPQUFRLENBQUE7QUFDbEIsMkJBQVEsR0FBRyxJQUFLLENBQUE7QUFDaEIsOEJBQVcsR0FBRyxRQUFTLENBQUE7QUFFdkIsMEJBQU8sR0FBRyxLQUFNLENBQUE7b0ZBTHBCLGtCQUFrQjt3RUFBbEIsa0JBQWtCLFdBQWxCLGtCQUFrQixtQkFGakIsTUFBTTt1RkFFUCxrQkFBa0I7Y0FIOUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDbGlja01vZHVsZU9wdGlvbnMge1xuICBwcml2YXRlIHN0YXRpYyBfYXBpVXJsID0gJy9hcGkvJztcbiAgcHJpdmF0ZSBzdGF0aWMgX2xvZ2dpbmcgPSB0cnVlO1xuICBwcml2YXRlIHN0YXRpYyBfbG9naW5Sb3V0ZSA9ICcvbG9naW4nO1xuICBwcml2YXRlIHN0YXRpYyBfYXBpU2VjcmV0S2V5O1xuICBwcml2YXRlIHN0YXRpYyBfbG9ja2VkID0gZmFsc2U7XG5cbiAgc3RhdGljIHNldE9wdGlvbnMob3B0aW9uczogb2JqZWN0KSB7XG4gICAgaWYgKG9wdGlvbnMpIHtcbiAgICAgIGZvciAoY29uc3QgcHJvcCBpbiBvcHRpb25zKSB7XG4gICAgICAgIENsaWNrTW9kdWxlT3B0aW9uc1twcm9wXSA9IG9wdGlvbnNbcHJvcF07XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGdldCBhcGlVcmwoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5fYXBpVXJsO1xuICB9XG5cbiAgc3RhdGljIHNldCBhcGlVcmwodmFsdWU6IHN0cmluZykge1xuICAgIGlmICghQ2xpY2tNb2R1bGVPcHRpb25zLl9sb2NrZWQpIHtcbiAgICAgIHRoaXMuX2FwaVVybCA9IHZhbHVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NsaWNrIE1vZHVsZSBPcHRpb25zIGhhdmUgYmVlbiBpbml0aWFsaXplZCBhbmQgbG9ja2VkLiBZb3UgY2Fubm90IGNoYW5nZSB0aGVtIG5vdycpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBnZXQgbG9nZ2luZygpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5fbG9nZ2luZztcbiAgfVxuXG4gIHN0YXRpYyBzZXQgbG9nZ2luZyh2YWx1ZTogYm9vbGVhbikge1xuICAgIGlmICghQ2xpY2tNb2R1bGVPcHRpb25zLl9sb2NrZWQpIHtcbiAgICAgIHRoaXMuX2xvZ2dpbmcgPSB2YWx1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdDbGljayBNb2R1bGUgT3B0aW9ucyBoYXZlIGJlZW4gaW5pdGlhbGl6ZWQgYW5kIGxvY2tlZC4gWW91IGNhbm5vdCBjaGFuZ2UgdGhlbSBub3cnKTtcbiAgICB9XG4gIH1cblxuICBzdGF0aWMgZ2V0IGxvZ2luUm91dGUoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5fbG9naW5Sb3V0ZTtcbiAgfVxuXG4gIHN0YXRpYyBzZXQgbG9naW5Sb3V0ZSh2YWx1ZTogc3RyaW5nKSB7XG4gICAgaWYgKCFDbGlja01vZHVsZU9wdGlvbnMuX2xvY2tlZCkge1xuICAgICAgdGhpcy5fbG9naW5Sb3V0ZSA9IHZhbHVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NsaWNrIE1vZHVsZSBPcHRpb25zIGhhdmUgYmVlbiBpbml0aWFsaXplZCBhbmQgbG9ja2VkLiBZb3UgY2Fubm90IGNoYW5nZSB0aGVtIG5vdycpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBnZXQgYXBpU2VjcmV0S2V5KCkge1xuICAgIHJldHVybiB0aGlzLl9hcGlTZWNyZXRLZXk7XG4gIH1cblxuICBzdGF0aWMgc2V0IGFwaVNlY3JldEtleSh2YWx1ZSkge1xuICAgIGlmICghQ2xpY2tNb2R1bGVPcHRpb25zLl9sb2NrZWQpIHtcbiAgICAgIHRoaXMuX2FwaVNlY3JldEtleSA9IHZhbHVlO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NsaWNrIE1vZHVsZSBPcHRpb25zIGhhdmUgYmVlbiBpbml0aWFsaXplZCBhbmQgbG9ja2VkLiBZb3UgY2Fubm90IGNoYW5nZSB0aGVtIG5vdycpO1xuICAgIH1cbiAgfVxuXG4gIHN0YXRpYyBsb2NrKCkge1xuICAgIENsaWNrTW9kdWxlT3B0aW9ucy5fbG9ja2VkID0gdHJ1ZTtcbiAgfVxufVxuIl19