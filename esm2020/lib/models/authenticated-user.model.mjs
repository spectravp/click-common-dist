import { ClickModuleOptions } from '../click.module';
import CryptoES from 'crypto-es';
import { EventEmitter } from '@angular/core';
import { AuthenticatedUserEvent } from './authenticated-user-event.model';
export class AuthenticatedUserModel {
    /**
     * Builds the user object
     * @param apiResponse The raw api response from httpClient
     */
    constructor(apiResponse = null) {
        if (!ClickModuleOptions.apiSecretKey) {
            throw new Error('You have not set a secret key for the API. Make sure this is unique for each project and can be random');
        }
        if (!AuthenticatedUserModel.onChangesEventEmitter) {
            AuthenticatedUserModel.onChangesEventEmitter = new EventEmitter();
        }
        if (apiResponse !== null) {
            AuthenticatedUserModel.id = apiResponse.user_id;
            AuthenticatedUserModel.accessToken = apiResponse.access_token;
            AuthenticatedUserModel.expires = apiResponse.expires;
            AuthenticatedUserModel.expirationDate = new Date(apiResponse.expires * 1000);
            AuthenticatedUserModel.scope = apiResponse.scope;
            AuthenticatedUserModel.dataLoaded = true;
            window.localStorage.setItem(ClickModuleOptions.apiSecretKey, CryptoES.AES.encrypt(JSON.stringify(apiResponse), AuthenticatedUserModel.encryptSecretKey).toString());
            this.emitChanges(AuthenticatedUserEvent.AUTHENTICATED);
        }
        else if (AuthenticatedUserModel.dataLoaded === false) {
            const data = window.localStorage.getItem(ClickModuleOptions.apiSecretKey);
            if (data !== null) {
                try {
                    const bytes = CryptoES.AES.decrypt(data, AuthenticatedUserModel.encryptSecretKey);
                    if (bytes.toString()) {
                        const obj = JSON.parse(bytes.toString(CryptoES.enc.Utf8));
                        AuthenticatedUserModel.id = obj.user_id;
                        AuthenticatedUserModel.accessToken = obj.access_token;
                        AuthenticatedUserModel.expires = obj.expires;
                        AuthenticatedUserModel.expirationDate = new Date(obj.expires * 1000);
                        AuthenticatedUserModel.scope = obj.scope;
                        AuthenticatedUserModel.dataLoaded = true;
                        this.emitChanges(AuthenticatedUserEvent.AUTHENTICATED);
                    }
                }
                catch (e) {
                    console.error('Decryption Error', e);
                }
            }
        }
        // Listen for changes to the storage and test validity
        if (!AuthenticatedUserModel.storageEventListenerRegistered) {
            AuthenticatedUserModel.storageEventListenerRegistered = true;
            window.addEventListener('storage', () => {
                this.isValid();
            });
        }
    }
    /**
     * Destroys the user
     */
    destroyUser() {
        AuthenticatedUserModel.id = null;
        AuthenticatedUserModel.accessToken = null;
        AuthenticatedUserModel.expirationDate = null;
        AuthenticatedUserModel.expires = null;
        AuthenticatedUserModel.scope = null;
        AuthenticatedUserModel.dataLoaded = false;
        window.localStorage.removeItem(ClickModuleOptions.apiSecretKey);
        this.emitChanges(AuthenticatedUserEvent.DEAUTHENTICATED);
    }
    /**
     * Gets the users ID
     */
    get id() {
        return AuthenticatedUserModel.id;
    }
    /**
     * Gets the access token
     */
    get accessToken() {
        return AuthenticatedUserModel.accessToken;
    }
    /**
     * Gets the expiration Date
     */
    get expirationDate() {
        return AuthenticatedUserModel.expirationDate;
    }
    /**
     * Gets the scope
     */
    get scope() {
        return AuthenticatedUserModel.scope;
    }
    /**
     * Gets the raw expires value
     */
    get expires() {
        return AuthenticatedUserModel.expires;
    }
    /**
     * Tests if it has a valid user
     */
    isValid() {
        if (!AuthenticatedUserModel.accessToken || !AuthenticatedUserModel.expirationDate || !AuthenticatedUserModel.scope) {
            // We are missing critical information
            return false;
        }
        const now = new Date();
        if (now > AuthenticatedUserModel.expirationDate) {
            // The token has expired
            return false;
        }
        // If we don't have any keys in storage, we cannot be logged in or we are dead on refresh
        if (!window.localStorage.getItem(ClickModuleOptions.apiSecretKey)) {
            // we don't have anything in storage
            return false;
        }
        return true;
    }
    /**
     * Gets a copy as an object
     */
    getCopy() {
        return JSON.parse(this.getJson());
    }
    /**
     * Gets the data as a JSON string
     */
    getJson() {
        return JSON.stringify({
            id: AuthenticatedUserModel.id,
            accessToken: AuthenticatedUserModel.accessToken,
            expires: AuthenticatedUserModel.expires,
            expirationDate: AuthenticatedUserModel.expirationDate,
            scope: AuthenticatedUserModel.scope,
        });
    }
    /**
     * Gets the on changes event listener
     */
    get onChanges() {
        return AuthenticatedUserModel.onChangesEventEmitter;
    }
    emitChanges(type) {
        AuthenticatedUserModel.onChangesEventEmitter.emit({ type, model: this });
    }
}
/**
 * Flag for whether data has been loaded from local storage
 */
AuthenticatedUserModel.dataLoaded = false;
/**
 * The id of the user record
 */
AuthenticatedUserModel.id = null;
/**
 * The access token of the user
 */
AuthenticatedUserModel.accessToken = null;
/**
 * The expiration date of the user
 */
AuthenticatedUserModel.expirationDate = null;
/**
 * The raw expiration time
 */
AuthenticatedUserModel.expires = null;
/**
 * The scope of the token
 */
AuthenticatedUserModel.scope = null;
/**
 * Encryption string for storage
 */
AuthenticatedUserModel.encryptSecretKey = 'fhjsdg,javb.g7lfty4rqg,d';
/**
 * Storage Event registered flag
 */
AuthenticatedUserModel.storageEventListenerRegistered = false;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRlZC11c2VyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY2xpY2svc3JjL2xpYi9tb2RlbHMvYXV0aGVudGljYXRlZC11c2VyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQ25ELE9BQU8sUUFBUSxNQUFNLFdBQVcsQ0FBQztBQUNqQyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLGtDQUFrQyxDQUFDO0FBRXhFLE1BQU0sT0FBTyxzQkFBc0I7SUFnRGpDOzs7T0FHRztJQUNILFlBQWEsY0FBMkQsSUFBSTtRQUMxRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFO1lBQ3BDLE1BQU0sSUFBSSxLQUFLLENBQUMsd0dBQXdHLENBQUMsQ0FBQztTQUMzSDtRQUVELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsRUFBRTtZQUNqRCxzQkFBc0IsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1NBQ25FO1FBRUQsSUFBSSxXQUFXLEtBQUssSUFBSSxFQUFHO1lBQ3pCLHNCQUFzQixDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ2hELHNCQUFzQixDQUFDLFdBQVcsR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDO1lBQzlELHNCQUFzQixDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO1lBQ3JELHNCQUFzQixDQUFDLGNBQWMsR0FBRyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDO1lBQzdFLHNCQUFzQixDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQ2pELHNCQUFzQixDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDekMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUMvRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFLHNCQUFzQixDQUFDLGdCQUFnQixDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNwRixJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3hEO2FBQU0sSUFBSSxzQkFBc0IsQ0FBQyxVQUFVLEtBQUssS0FBSyxFQUFHO1lBQ3ZELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRTFFLElBQUksSUFBSSxLQUFLLElBQUksRUFBRztnQkFDbEIsSUFBSTtvQkFDRixNQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsc0JBQXNCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDbEYsSUFBSSxLQUFLLENBQUMsUUFBUSxFQUFFLEVBQUU7d0JBQ3BCLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQzFELHNCQUFzQixDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO3dCQUN4QyxzQkFBc0IsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQzt3QkFDdEQsc0JBQXNCLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7d0JBQzdDLHNCQUFzQixDQUFDLGNBQWMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxDQUFDO3dCQUNyRSxzQkFBc0IsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQzt3QkFDekMsc0JBQXNCLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzt3QkFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztxQkFDeEQ7aUJBQ0Y7Z0JBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ1YsT0FBTyxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDdEM7YUFDRjtTQUNGO1FBRUQsc0RBQXNEO1FBQ3RELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyw4QkFBOEIsRUFBRTtZQUMxRCxzQkFBc0IsQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUM7WUFDN0QsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxHQUFHLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUNqQixDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUdEOztPQUVHO0lBQ0gsV0FBVztRQUNULHNCQUFzQixDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDakMsc0JBQXNCLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUMxQyxzQkFBc0IsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzdDLHNCQUFzQixDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDdEMsc0JBQXNCLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztRQUNwQyxzQkFBc0IsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxFQUFFO1FBQ0osT0FBTyxzQkFBc0IsQ0FBQyxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxXQUFXO1FBQ2IsT0FBTyxzQkFBc0IsQ0FBQyxXQUFXLENBQUM7SUFDNUMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxjQUFjO1FBQ2hCLE9BQU8sc0JBQXNCLENBQUMsY0FBYyxDQUFDO0lBQy9DLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUksS0FBSztRQUNQLE9BQU8sc0JBQXNCLENBQUMsS0FBSyxDQUFDO0lBQ3RDLENBQUM7SUFFRDs7T0FFRztJQUNILElBQUksT0FBTztRQUNULE9BQU8sc0JBQXNCLENBQUMsT0FBTyxDQUFDO0lBQ3hDLENBQUM7SUFFRDs7T0FFRztJQUNILE9BQU87UUFDTCxJQUFJLENBQUUsc0JBQXNCLENBQUMsV0FBVyxJQUFJLENBQUUsc0JBQXNCLENBQUMsY0FBYyxJQUFJLENBQUUsc0JBQXNCLENBQUMsS0FBSyxFQUFHO1lBQ3RILHNDQUFzQztZQUN0QyxPQUFPLEtBQUssQ0FBQztTQUNkO1FBRUQsTUFBTSxHQUFHLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUV2QixJQUFJLEdBQUcsR0FBRyxzQkFBc0IsQ0FBQyxjQUFjLEVBQUc7WUFDaEQsd0JBQXdCO1lBQ3hCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCx5RkFBeUY7UUFDekYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ2pFLG9DQUFvQztZQUNwQyxPQUFPLEtBQUssQ0FBQztTQUNkO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxPQUFPO1FBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7T0FFRztJQUNILE9BQU87UUFDTCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDcEIsRUFBRSxFQUFFLHNCQUFzQixDQUFDLEVBQUU7WUFDN0IsV0FBVyxFQUFFLHNCQUFzQixDQUFDLFdBQVc7WUFDL0MsT0FBTyxFQUFFLHNCQUFzQixDQUFDLE9BQU87WUFDdkMsY0FBYyxFQUFFLHNCQUFzQixDQUFDLGNBQWM7WUFDckQsS0FBSyxFQUFFLHNCQUFzQixDQUFDLEtBQUs7U0FDcEMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSSxTQUFTO1FBQ1gsT0FBTyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQztJQUN0RCxDQUFDO0lBRU8sV0FBVyxDQUFDLElBQVk7UUFDOUIsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7O0FBNU1EOztHQUVHO0FBQ1ksaUNBQVUsR0FBRyxLQUFLLENBQUM7QUFFbEM7O0dBRUc7QUFDWSx5QkFBRSxHQUFXLElBQUksQ0FBQztBQUVqQzs7R0FFRztBQUNZLGtDQUFXLEdBQVcsSUFBSSxDQUFDO0FBRTFDOztHQUVHO0FBQ1kscUNBQWMsR0FBUyxJQUFJLENBQUM7QUFFM0M7O0dBRUc7QUFDWSw4QkFBTyxHQUFXLElBQUksQ0FBQztBQUV0Qzs7R0FFRztBQUNZLDRCQUFLLEdBQVcsSUFBSSxDQUFDO0FBRXBDOztHQUVHO0FBQ1ksdUNBQWdCLEdBQUcsMEJBQTBCLENBQUM7QUFFN0Q7O0dBRUc7QUFDWSxxREFBOEIsR0FBRyxLQUFLLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0FwaUF1dGhlbnRpY2F0aW9uUmVzcG9uc2VNb2RlbH0gZnJvbSAnLi9hcGktYXV0aGVudGljYXRpb24tcmVzcG9uc2UubW9kZWwnO1xuaW1wb3J0IHtDbGlja01vZHVsZU9wdGlvbnN9IGZyb20gJy4uL2NsaWNrLm1vZHVsZSc7XG5pbXBvcnQgQ3J5cHRvRVMgZnJvbSAnY3J5cHRvLWVzJztcbmltcG9ydCB7RXZlbnRFbWl0dGVyfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7QXV0aGVudGljYXRlZFVzZXJFdmVudH0gZnJvbSAnLi9hdXRoZW50aWNhdGVkLXVzZXItZXZlbnQubW9kZWwnO1xuXG5leHBvcnQgY2xhc3MgQXV0aGVudGljYXRlZFVzZXJNb2RlbCB7XG5cbiAgLyoqXG4gICAqIEZsYWcgZm9yIHdoZXRoZXIgZGF0YSBoYXMgYmVlbiBsb2FkZWQgZnJvbSBsb2NhbCBzdG9yYWdlXG4gICAqL1xuICBwcml2YXRlIHN0YXRpYyBkYXRhTG9hZGVkID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFRoZSBpZCBvZiB0aGUgdXNlciByZWNvcmRcbiAgICovXG4gIHByaXZhdGUgc3RhdGljIGlkOiBzdHJpbmcgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBUaGUgYWNjZXNzIHRva2VuIG9mIHRoZSB1c2VyXG4gICAqL1xuICBwcml2YXRlIHN0YXRpYyBhY2Nlc3NUb2tlbjogc3RyaW5nID0gbnVsbDtcblxuICAvKipcbiAgICogVGhlIGV4cGlyYXRpb24gZGF0ZSBvZiB0aGUgdXNlclxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgZXhwaXJhdGlvbkRhdGU6IERhdGUgPSBudWxsO1xuXG4gIC8qKlxuICAgKiBUaGUgcmF3IGV4cGlyYXRpb24gdGltZVxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgZXhwaXJlczogbnVtYmVyID0gbnVsbDtcblxuICAvKipcbiAgICogVGhlIHNjb3BlIG9mIHRoZSB0b2tlblxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgc2NvcGU6IHN0cmluZyA9IG51bGw7XG5cbiAgLyoqXG4gICAqIEVuY3J5cHRpb24gc3RyaW5nIGZvciBzdG9yYWdlXG4gICAqL1xuICBwcml2YXRlIHN0YXRpYyBlbmNyeXB0U2VjcmV0S2V5ID0gJ2ZoanNkZyxqYXZiLmc3bGZ0eTRycWcsZCc7XG5cbiAgLyoqXG4gICAqIFN0b3JhZ2UgRXZlbnQgcmVnaXN0ZXJlZCBmbGFnXG4gICAqL1xuICBwcml2YXRlIHN0YXRpYyBzdG9yYWdlRXZlbnRMaXN0ZW5lclJlZ2lzdGVyZWQgPSBmYWxzZTtcblxuICAvKipcbiAgICogQW4gZXZlbnQgZW1pdHRlciBzbyB5b3UgY2FuIGxpc3RlbiBmb3IgYXV0aGVudGljYXRpb24gY2hhbmdlc1xuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcHJpdmF0ZSBzdGF0aWMgb25DaGFuZ2VzRXZlbnRFbWl0dGVyOiBFdmVudEVtaXR0ZXI8QXV0aGVudGljYXRlZFVzZXJFdmVudD47XG5cbiAgLyoqXG4gICAqIEJ1aWxkcyB0aGUgdXNlciBvYmplY3RcbiAgICogQHBhcmFtIGFwaVJlc3BvbnNlIFRoZSByYXcgYXBpIHJlc3BvbnNlIGZyb20gaHR0cENsaWVudFxuICAgKi9cbiAgY29uc3RydWN0b3IoIGFwaVJlc3BvbnNlOiBBcGlBdXRoZW50aWNhdGlvblJlc3BvbnNlTW9kZWwgfCBhbnkgfCBudWxsID0gbnVsbCkge1xuICAgIGlmICghQ2xpY2tNb2R1bGVPcHRpb25zLmFwaVNlY3JldEtleSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdZb3UgaGF2ZSBub3Qgc2V0IGEgc2VjcmV0IGtleSBmb3IgdGhlIEFQSS4gTWFrZSBzdXJlIHRoaXMgaXMgdW5pcXVlIGZvciBlYWNoIHByb2plY3QgYW5kIGNhbiBiZSByYW5kb20nKTtcbiAgICB9XG5cbiAgICBpZiAoIUF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwub25DaGFuZ2VzRXZlbnRFbWl0dGVyKSB7XG4gICAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLm9uQ2hhbmdlc0V2ZW50RW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICB9XG5cbiAgICBpZiAoYXBpUmVzcG9uc2UgIT09IG51bGwgKSB7XG4gICAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmlkID0gYXBpUmVzcG9uc2UudXNlcl9pZDtcbiAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuYWNjZXNzVG9rZW4gPSBhcGlSZXNwb25zZS5hY2Nlc3NfdG9rZW47XG4gICAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyZXMgPSBhcGlSZXNwb25zZS5leHBpcmVzO1xuICAgICAgQXV0aGVudGljYXRlZFVzZXJNb2RlbC5leHBpcmF0aW9uRGF0ZSA9IG5ldyBEYXRlKGFwaVJlc3BvbnNlLmV4cGlyZXMgKiAxMDAwKTtcbiAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuc2NvcGUgPSBhcGlSZXNwb25zZS5zY29wZTtcbiAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZGF0YUxvYWRlZCA9IHRydWU7XG4gICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oQ2xpY2tNb2R1bGVPcHRpb25zLmFwaVNlY3JldEtleSwgQ3J5cHRvRVMuQUVTLmVuY3J5cHQoXG4gICAgICAgIEpTT04uc3RyaW5naWZ5KGFwaVJlc3BvbnNlKSwgQXV0aGVudGljYXRlZFVzZXJNb2RlbC5lbmNyeXB0U2VjcmV0S2V5KS50b1N0cmluZygpKTtcbiAgICAgIHRoaXMuZW1pdENoYW5nZXMoQXV0aGVudGljYXRlZFVzZXJFdmVudC5BVVRIRU5USUNBVEVEKTtcbiAgICB9IGVsc2UgaWYgKEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZGF0YUxvYWRlZCA9PT0gZmFsc2UgKSB7XG4gICAgICBjb25zdCBkYXRhID0gd2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKENsaWNrTW9kdWxlT3B0aW9ucy5hcGlTZWNyZXRLZXkpO1xuXG4gICAgICBpZiAoZGF0YSAhPT0gbnVsbCApIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICBjb25zdCBieXRlcyA9IENyeXB0b0VTLkFFUy5kZWNyeXB0KGRhdGEsIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZW5jcnlwdFNlY3JldEtleSk7XG4gICAgICAgICAgaWYgKGJ5dGVzLnRvU3RyaW5nKCkpIHtcbiAgICAgICAgICAgIGNvbnN0IG9iaiA9IEpTT04ucGFyc2UoYnl0ZXMudG9TdHJpbmcoQ3J5cHRvRVMuZW5jLlV0ZjgpKTtcbiAgICAgICAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuaWQgPSBvYmoudXNlcl9pZDtcbiAgICAgICAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuYWNjZXNzVG9rZW4gPSBvYmouYWNjZXNzX3Rva2VuO1xuICAgICAgICAgICAgQXV0aGVudGljYXRlZFVzZXJNb2RlbC5leHBpcmVzID0gb2JqLmV4cGlyZXM7XG4gICAgICAgICAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyYXRpb25EYXRlID0gbmV3IERhdGUob2JqLmV4cGlyZXMgKiAxMDAwKTtcbiAgICAgICAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuc2NvcGUgPSBvYmouc2NvcGU7XG4gICAgICAgICAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmRhdGFMb2FkZWQgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5lbWl0Q2hhbmdlcyhBdXRoZW50aWNhdGVkVXNlckV2ZW50LkFVVEhFTlRJQ0FURUQpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0RlY3J5cHRpb24gRXJyb3InLCBlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIExpc3RlbiBmb3IgY2hhbmdlcyB0byB0aGUgc3RvcmFnZSBhbmQgdGVzdCB2YWxpZGl0eVxuICAgIGlmICghQXV0aGVudGljYXRlZFVzZXJNb2RlbC5zdG9yYWdlRXZlbnRMaXN0ZW5lclJlZ2lzdGVyZWQpIHtcbiAgICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuc3RvcmFnZUV2ZW50TGlzdGVuZXJSZWdpc3RlcmVkID0gdHJ1ZTtcbiAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzdG9yYWdlJywgKCkgPT4ge1xuICAgICAgICB0aGlzLmlzVmFsaWQoKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG5cbiAgLyoqXG4gICAqIERlc3Ryb3lzIHRoZSB1c2VyXG4gICAqL1xuICBkZXN0cm95VXNlcigpIHtcbiAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmlkID0gbnVsbDtcbiAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmFjY2Vzc1Rva2VuID0gbnVsbDtcbiAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyYXRpb25EYXRlID0gbnVsbDtcbiAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyZXMgPSBudWxsO1xuICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuc2NvcGUgPSBudWxsO1xuICAgIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZGF0YUxvYWRlZCA9IGZhbHNlO1xuICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShDbGlja01vZHVsZU9wdGlvbnMuYXBpU2VjcmV0S2V5KTtcbiAgICB0aGlzLmVtaXRDaGFuZ2VzKEF1dGhlbnRpY2F0ZWRVc2VyRXZlbnQuREVBVVRIRU5USUNBVEVEKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSB1c2VycyBJRFxuICAgKi9cbiAgZ2V0IGlkKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuaWQ7XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgYWNjZXNzIHRva2VuXG4gICAqL1xuICBnZXQgYWNjZXNzVG9rZW4oKTogc3RyaW5nIHtcbiAgICByZXR1cm4gQXV0aGVudGljYXRlZFVzZXJNb2RlbC5hY2Nlc3NUb2tlbjtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBleHBpcmF0aW9uIERhdGVcbiAgICovXG4gIGdldCBleHBpcmF0aW9uRGF0ZSgpOiBEYXRlIHtcbiAgICByZXR1cm4gQXV0aGVudGljYXRlZFVzZXJNb2RlbC5leHBpcmF0aW9uRGF0ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBzY29wZVxuICAgKi9cbiAgZ2V0IHNjb3BlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuc2NvcGU7XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgcmF3IGV4cGlyZXMgdmFsdWVcbiAgICovXG4gIGdldCBleHBpcmVzKCk6IG51bWJlciB7XG4gICAgcmV0dXJuIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZXhwaXJlcztcbiAgfVxuXG4gIC8qKlxuICAgKiBUZXN0cyBpZiBpdCBoYXMgYSB2YWxpZCB1c2VyXG4gICAqL1xuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xuICAgIGlmICghIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuYWNjZXNzVG9rZW4gfHwgISBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyYXRpb25EYXRlIHx8ICEgQXV0aGVudGljYXRlZFVzZXJNb2RlbC5zY29wZSApIHtcbiAgICAgIC8vIFdlIGFyZSBtaXNzaW5nIGNyaXRpY2FsIGluZm9ybWF0aW9uXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY29uc3Qgbm93ID0gbmV3IERhdGUoKTtcblxuICAgIGlmIChub3cgPiBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmV4cGlyYXRpb25EYXRlICkge1xuICAgICAgLy8gVGhlIHRva2VuIGhhcyBleHBpcmVkXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gSWYgd2UgZG9uJ3QgaGF2ZSBhbnkga2V5cyBpbiBzdG9yYWdlLCB3ZSBjYW5ub3QgYmUgbG9nZ2VkIGluIG9yIHdlIGFyZSBkZWFkIG9uIHJlZnJlc2hcbiAgICBpZiAoIXdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbShDbGlja01vZHVsZU9wdGlvbnMuYXBpU2VjcmV0S2V5KSkge1xuICAgICAgLy8gd2UgZG9uJ3QgaGF2ZSBhbnl0aGluZyBpbiBzdG9yYWdlXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICAvKipcbiAgICogR2V0cyBhIGNvcHkgYXMgYW4gb2JqZWN0XG4gICAqL1xuICBnZXRDb3B5KCk6IHtpZDogc3RyaW5nLCBhY2Nlc3NUb2tlbjogc3RyaW5nLCBleHBpcmF0aW9uRGF0ZTogc3RyaW5nLCBzY29wZTogc3RyaW5nfSB7XG4gICAgcmV0dXJuIEpTT04ucGFyc2UodGhpcy5nZXRKc29uKCkpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGRhdGEgYXMgYSBKU09OIHN0cmluZ1xuICAgKi9cbiAgZ2V0SnNvbigpOiBzdHJpbmcge1xuICAgIHJldHVybiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICBpZDogQXV0aGVudGljYXRlZFVzZXJNb2RlbC5pZCxcbiAgICAgIGFjY2Vzc1Rva2VuOiBBdXRoZW50aWNhdGVkVXNlck1vZGVsLmFjY2Vzc1Rva2VuLFxuICAgICAgZXhwaXJlczogQXV0aGVudGljYXRlZFVzZXJNb2RlbC5leHBpcmVzLFxuICAgICAgZXhwaXJhdGlvbkRhdGU6IEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwuZXhwaXJhdGlvbkRhdGUsXG4gICAgICBzY29wZTogQXV0aGVudGljYXRlZFVzZXJNb2RlbC5zY29wZSxcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXRzIHRoZSBvbiBjaGFuZ2VzIGV2ZW50IGxpc3RlbmVyXG4gICAqL1xuICBnZXQgb25DaGFuZ2VzKCk6IEV2ZW50RW1pdHRlcjxBdXRoZW50aWNhdGVkVXNlckV2ZW50PiB7XG4gICAgcmV0dXJuIEF1dGhlbnRpY2F0ZWRVc2VyTW9kZWwub25DaGFuZ2VzRXZlbnRFbWl0dGVyO1xuICB9XG5cbiAgcHJpdmF0ZSBlbWl0Q2hhbmdlcyh0eXBlOiBzdHJpbmcpIHtcbiAgICBBdXRoZW50aWNhdGVkVXNlck1vZGVsLm9uQ2hhbmdlc0V2ZW50RW1pdHRlci5lbWl0KHt0eXBlLCBtb2RlbDogdGhpc30pO1xuICB9XG59XG4iXX0=