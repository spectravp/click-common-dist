import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class UnsavedWorkGuard {
    canDeactivate(component, currentRoute, currentState, nextState) {
        if (!component.canDeactivate()) {
            if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }
}
UnsavedWorkGuard.ɵfac = function UnsavedWorkGuard_Factory(t) { return new (t || UnsavedWorkGuard)(); };
UnsavedWorkGuard.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: UnsavedWorkGuard, factory: UnsavedWorkGuard.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(UnsavedWorkGuard, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5zYXZlZC13b3JrLmd1YXJkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvY2xpY2svc3JjL2xpYi9ndWFyZHMvdW5zYXZlZC13b3JrLmd1YXJkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBUTNDLE1BQU0sT0FBTyxnQkFBZ0I7SUFDM0IsYUFBYSxDQUFDLFNBQWdDLEVBQ2hDLFlBQW9DLEVBQ3BDLFlBQWlDLEVBQ2pDLFNBQStCO1FBQzNDLElBQUssQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUc7WUFDaEMsSUFBSSxPQUFPLENBQUMsb0VBQW9FLENBQUMsRUFBRTtnQkFDakYsT0FBTyxJQUFJLENBQUM7YUFDYjtpQkFBTTtnQkFDTCxPQUFPLEtBQUssQ0FBQzthQUNkO1NBQ0Y7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7O2dGQWJVLGdCQUFnQjtzRUFBaEIsZ0JBQWdCLFdBQWhCLGdCQUFnQixtQkFGZixNQUFNO3VGQUVQLGdCQUFnQjtjQUg1QixVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBDYW5EZWFjdGl2YXRlLCBSb3V0ZXJTdGF0ZVNuYXBzaG90LCBVcmxUcmVlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7QWJzdHJhY3RGb3JtQ29tcG9uZW50fSBmcm9tICcuLi9mb3Jtcy9hYnN0cmFjdC1mb3JtLmNvbXBvbmVudCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFVuc2F2ZWRXb3JrR3VhcmQgaW1wbGVtZW50cyBDYW5EZWFjdGl2YXRlPEFic3RyYWN0Rm9ybUNvbXBvbmVudD4ge1xuICBjYW5EZWFjdGl2YXRlKGNvbXBvbmVudDogQWJzdHJhY3RGb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGN1cnJlbnRSb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcbiAgICAgICAgICAgICAgICBjdXJyZW50U3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QsXG4gICAgICAgICAgICAgICAgbmV4dFN0YXRlPzogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbiB8IFVybFRyZWU+IHwgUHJvbWlzZTxib29sZWFuIHwgVXJsVHJlZT4gfCBib29sZWFuIHwgVXJsVHJlZSB7XG4gICAgaWYgKCAhY29tcG9uZW50LmNhbkRlYWN0aXZhdGUoKSApIHtcbiAgICAgIGlmIChjb25maXJtKCdZb3UgaGF2ZSB1bnNhdmVkIGNoYW5nZXMhIElmIHlvdSBsZWF2ZSwgeW91ciBjaGFuZ2VzIHdpbGwgYmUgbG9zdC4nKSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn1cbiJdfQ==