import { Injectable } from '@angular/core';
import { AuthenticatedUserModel } from '../models/authenticated-user.model';
import { ClickModuleOptions } from '../models/click-module-options.model';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
export class AuthenticatedGuard {
    /**
     * Constructor
     */
    constructor(router) {
        this.router = router;
        this.user = new AuthenticatedUserModel();
    }
    /**
     * Can Activate
     */
    canActivate(next, state) {
        return this.guard();
    }
    /**
     * Can Activate Child
     */
    canActivateChild(next, state) {
        return this.guard();
    }
    /**
     * Can Load
     */
    canLoad(route, segments) {
        return this.guard();
    }
    /**
     * Tests if the user is valid and redirects if route is not null
     */
    guard() {
        if (!this.user.isValid()) {
            if (ClickModuleOptions.loginRoute !== null) {
                this.router.navigateByUrl(ClickModuleOptions.loginRoute);
            }
            return false;
        }
        return true;
    }
}
AuthenticatedGuard.ɵfac = function AuthenticatedGuard_Factory(t) { return new (t || AuthenticatedGuard)(i0.ɵɵinject(i1.Router)); };
AuthenticatedGuard.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthenticatedGuard, factory: AuthenticatedGuard.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthenticatedGuard, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.Router }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRlZC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvZ3VhcmRzL2F1dGhlbnRpY2F0ZWQuZ3VhcmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQWEzQyxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUMxRSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQzs7O0FBS3hFLE1BQU0sT0FBTyxrQkFBa0I7SUFNN0I7O09BRUc7SUFDSCxZQUFvQixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNoQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksc0JBQXNCLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxXQUFXLENBQ1QsSUFBNEIsRUFDNUIsS0FBMEI7UUFDMUIsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0JBQWdCLENBQ2QsSUFBNEIsRUFDNUIsS0FBMEI7UUFDMUIsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsT0FBTyxDQUNMLEtBQVksRUFDWixRQUFzQjtRQUN0QixPQUFPLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQ7O09BRUc7SUFDSyxLQUFLO1FBQ1gsSUFBSyxDQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUk7WUFDNUIsSUFBSSxrQkFBa0IsQ0FBQyxVQUFVLEtBQUssSUFBSSxFQUFHO2dCQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUMxRDtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7O29GQXBEVSxrQkFBa0I7d0VBQWxCLGtCQUFrQixXQUFsQixrQkFBa0IsbUJBRmpCLE1BQU07dUZBRVAsa0JBQWtCO2NBSDlCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7XG4gIENhbkFjdGl2YXRlLFxuICBDYW5BY3RpdmF0ZUNoaWxkLFxuICBDYW5Mb2FkLFxuICBSb3V0ZSxcbiAgVXJsU2VnbWVudCxcbiAgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcbiAgUm91dGVyU3RhdGVTbmFwc2hvdCxcbiAgVXJsVHJlZSxcbiAgUm91dGVyXG59IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0F1dGhlbnRpY2F0ZWRVc2VyTW9kZWx9IGZyb20gJy4uL21vZGVscy9hdXRoZW50aWNhdGVkLXVzZXIubW9kZWwnO1xuaW1wb3J0IHtDbGlja01vZHVsZU9wdGlvbnN9IGZyb20gJy4uL21vZGVscy9jbGljay1tb2R1bGUtb3B0aW9ucy5tb2RlbCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0ZWRHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlLCBDYW5BY3RpdmF0ZUNoaWxkLCBDYW5Mb2FkIHtcbiAgLyoqXG4gICAqIFRoZSBhdXRoZW50aWNhdGVkIHVzZXIgbW9kZWxcbiAgICovXG4gIHByaXZhdGUgdXNlcjogQXV0aGVudGljYXRlZFVzZXJNb2RlbDtcblxuICAvKipcbiAgICogQ29uc3RydWN0b3JcbiAgICovXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICB0aGlzLnVzZXIgPSBuZXcgQXV0aGVudGljYXRlZFVzZXJNb2RlbCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIENhbiBBY3RpdmF0ZVxuICAgKi9cbiAgY2FuQWN0aXZhdGUoXG4gICAgbmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcbiAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbiB8IFVybFRyZWU+IHwgUHJvbWlzZTxib29sZWFuIHwgVXJsVHJlZT4gfCBib29sZWFuIHwgVXJsVHJlZSB7XG4gICAgcmV0dXJuIHRoaXMuZ3VhcmQoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYW4gQWN0aXZhdGUgQ2hpbGRcbiAgICovXG4gIGNhbkFjdGl2YXRlQ2hpbGQoXG4gICAgbmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcbiAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IE9ic2VydmFibGU8Ym9vbGVhbiB8IFVybFRyZWU+IHwgUHJvbWlzZTxib29sZWFuIHwgVXJsVHJlZT4gfCBib29sZWFuIHwgVXJsVHJlZSB7XG4gICAgcmV0dXJuIHRoaXMuZ3VhcmQoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDYW4gTG9hZFxuICAgKi9cbiAgY2FuTG9hZChcbiAgICByb3V0ZTogUm91dGUsXG4gICAgc2VnbWVudHM6IFVybFNlZ21lbnRbXSk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMuZ3VhcmQoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUZXN0cyBpZiB0aGUgdXNlciBpcyB2YWxpZCBhbmQgcmVkaXJlY3RzIGlmIHJvdXRlIGlzIG5vdCBudWxsXG4gICAqL1xuICBwcml2YXRlIGd1YXJkKCk6IGJvb2xlYW4ge1xuICAgIGlmICggISB0aGlzLnVzZXIuaXNWYWxpZCgpICkgIHtcbiAgICAgIGlmIChDbGlja01vZHVsZU9wdGlvbnMubG9naW5Sb3V0ZSAhPT0gbnVsbCApIHtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChDbGlja01vZHVsZU9wdGlvbnMubG9naW5Sb3V0ZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn1cbiJdfQ==