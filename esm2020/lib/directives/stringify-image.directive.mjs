import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import * as i0 from "@angular/core";
export class StringifyImageDirective {
    // constructor
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
        // internal count of how many files were selected
        // used to determine if we need to be an array or not
        this.count = 0;
        // Setup an event emitter for changes to the model
        this.modelChange = new EventEmitter();
    }
    // Set up a change listener
    onChange(files) {
        if (files.length) {
            this.count = files.length;
            if (this.count > 1) {
                this.internalModel = [];
            }
            else {
                this.internalModel = '';
            }
            for (let i = 0; i < files.length; i++) {
                this.readFile(files[i]);
            }
        }
    }
    /**
     * Sets the model value and emits the change event
     */
    setModelValue(value) {
        if (this.internalModel instanceof Array) {
            this.internalModel.push(value);
        }
        else {
            this.internalModel = value;
        }
        if (this.model instanceof UntypedFormControl) {
            this.model.setValue(this.internalModel);
        }
        else {
            this.model = this.internalModel;
        }
        this.modelChange.emit(this.model);
    }
    /**
     * Reads the individual file
     */
    readFile(file) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = async (e) => {
            this.setModelValue(e.target.result);
        };
    }
}
StringifyImageDirective.ɵfac = function StringifyImageDirective_Factory(t) { return new (t || StringifyImageDirective)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i0.ElementRef)); };
StringifyImageDirective.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: StringifyImageDirective, selectors: [["", "clickStringifyImage", ""]], hostBindings: function StringifyImageDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("change", function StringifyImageDirective_change_HostBindingHandler($event) { return ctx.onChange($event.target.files); });
    } }, inputs: { model: "model" }, outputs: { modelChange: "modelChange" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(StringifyImageDirective, [{
        type: Directive,
        args: [{
                selector: '[clickStringifyImage]'
            }]
    }], function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; }, { model: [{
            type: Input
        }], modelChange: [{
            type: Output
        }], onChange: [{
            type: HostListener,
            args: ['change', ['$event.target.files']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaW5naWZ5LWltYWdlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvZGlyZWN0aXZlcy9zdHJpbmdpZnktaW1hZ2UuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQWMsWUFBWSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQzFHLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLGdCQUFnQixDQUFDOztBQUtsRCxNQUFNLE9BQU8sdUJBQXVCO0lBU2xDLGNBQWM7SUFDZCxZQUFvQixRQUFtQixFQUFVLEVBQWM7UUFBM0MsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUFVLE9BQUUsR0FBRixFQUFFLENBQVk7UUFSL0QsaURBQWlEO1FBQ2pELHFEQUFxRDtRQUM3QyxVQUFLLEdBQUcsQ0FBQyxDQUFDO1FBV2xCLGtEQUFrRDtRQUN4QyxnQkFBVyxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO0lBTkQsQ0FBQztJQVFuRSwyQkFBMkI7SUFFM0IsUUFBUSxDQUFDLEtBQUs7UUFDWixJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBRTFCLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2FBQ3pCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO2FBQ3pCO1lBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekI7U0FDRjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNLLGFBQWEsQ0FBQyxLQUFhO1FBQ2pDLElBQUksSUFBSSxDQUFDLGFBQWEsWUFBWSxLQUFLLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1NBQzVCO1FBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxZQUFZLGtCQUFrQixFQUFFO1lBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUN6QzthQUFNO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1NBQ2pDO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7T0FFRztJQUNLLFFBQVEsQ0FBQyxJQUFJO1FBQ25CLE1BQU0sVUFBVSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFDcEMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQixVQUFVLENBQUMsTUFBTSxHQUFHLEtBQUssRUFBRSxDQUFNLEVBQUUsRUFBRTtZQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDO0lBQ0osQ0FBQzs7OEZBaEVVLHVCQUF1QjswRUFBdkIsdUJBQXVCOzRHQUF2QixpQ0FFWDs7dUZBRlcsdUJBQXVCO2NBSG5DLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsdUJBQXVCO2FBQ2xDO3FGQWNVLEtBQUs7a0JBQWIsS0FBSztZQUdJLFdBQVc7a0JBQXBCLE1BQU07WUFJUCxRQUFRO2tCQURQLFlBQVk7bUJBQUMsUUFBUSxFQUFFLENBQUMscUJBQXFCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0RpcmVjdGl2ZSwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPdXRwdXQsIFJlbmRlcmVyMn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge1VudHlwZWRGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbY2xpY2tTdHJpbmdpZnlJbWFnZV0nXG59KVxuZXhwb3J0IGNsYXNzIFN0cmluZ2lmeUltYWdlRGlyZWN0aXZlIHtcblxuICAvLyBpbnRlcm5hbCBjb3VudCBvZiBob3cgbWFueSBmaWxlcyB3ZXJlIHNlbGVjdGVkXG4gIC8vIHVzZWQgdG8gZGV0ZXJtaW5lIGlmIHdlIG5lZWQgdG8gYmUgYW4gYXJyYXkgb3Igbm90XG4gIHByaXZhdGUgY291bnQgPSAwO1xuXG4gIC8vIGFuIGludGVybmFsIG1vZGVsXG4gIHByaXZhdGUgaW50ZXJuYWxNb2RlbDogc3RyaW5nfEFycmF5PHN0cmluZz47XG5cbiAgLy8gY29uc3RydWN0b3JcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLCBwcml2YXRlIGVsOiBFbGVtZW50UmVmKSB7fVxuXG4gIC8vIFNldCB1cCB0aGUgbW9kZWwgYXMgYW4gaW5wdXRcbiAgQElucHV0KCkgbW9kZWw6IGFueTtcblxuICAvLyBTZXR1cCBhbiBldmVudCBlbWl0dGVyIGZvciBjaGFuZ2VzIHRvIHRoZSBtb2RlbFxuICBAT3V0cHV0KCkgbW9kZWxDaGFuZ2U6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgLy8gU2V0IHVwIGEgY2hhbmdlIGxpc3RlbmVyXG4gIEBIb3N0TGlzdGVuZXIoJ2NoYW5nZScsIFsnJGV2ZW50LnRhcmdldC5maWxlcyddKVxuICBvbkNoYW5nZShmaWxlcykge1xuICAgIGlmIChmaWxlcy5sZW5ndGgpIHtcbiAgICAgIHRoaXMuY291bnQgPSBmaWxlcy5sZW5ndGg7XG5cbiAgICAgIGlmICh0aGlzLmNvdW50ID4gMSkge1xuICAgICAgICB0aGlzLmludGVybmFsTW9kZWwgPSBbXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuaW50ZXJuYWxNb2RlbCA9ICcnO1xuICAgICAgfVxuXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpbGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHRoaXMucmVhZEZpbGUoZmlsZXNbaV0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBtb2RlbCB2YWx1ZSBhbmQgZW1pdHMgdGhlIGNoYW5nZSBldmVudFxuICAgKi9cbiAgcHJpdmF0ZSBzZXRNb2RlbFZhbHVlKHZhbHVlOiBzdHJpbmcpIHtcbiAgICBpZiAodGhpcy5pbnRlcm5hbE1vZGVsIGluc3RhbmNlb2YgQXJyYXkpIHtcbiAgICAgIHRoaXMuaW50ZXJuYWxNb2RlbC5wdXNoKHZhbHVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5pbnRlcm5hbE1vZGVsID0gdmFsdWU7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMubW9kZWwgaW5zdGFuY2VvZiBVbnR5cGVkRm9ybUNvbnRyb2wpIHtcbiAgICAgIHRoaXMubW9kZWwuc2V0VmFsdWUodGhpcy5pbnRlcm5hbE1vZGVsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5tb2RlbCA9IHRoaXMuaW50ZXJuYWxNb2RlbDtcbiAgICB9XG5cbiAgICB0aGlzLm1vZGVsQ2hhbmdlLmVtaXQodGhpcy5tb2RlbCk7XG4gIH1cblxuICAvKipcbiAgICogUmVhZHMgdGhlIGluZGl2aWR1YWwgZmlsZVxuICAgKi9cbiAgcHJpdmF0ZSByZWFkRmlsZShmaWxlKSB7XG4gICAgY29uc3QgZmlsZVJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XG4gICAgZmlsZVJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgIGZpbGVSZWFkZXIub25sb2FkID0gYXN5bmMgKGU6IGFueSkgPT4ge1xuICAgICAgdGhpcy5zZXRNb2RlbFZhbHVlKGUudGFyZ2V0LnJlc3VsdCk7XG4gICAgfTtcbiAgfVxufVxuIl19