import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';
import * as i0 from "@angular/core";
export class DragDropFilesDirective {
    constructor() {
        this.fileDropped = new EventEmitter();
        this.cssClass = 'click-drag-drop-files';
    }
    // Dragover Event
    dragOver(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-over';
    }
    // Dragleave Event
    dragLeave(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-leave';
    }
    // Drop Event
    drop(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-drop';
        const files = event.dataTransfer.files;
        if (files.length > 0) {
            this.fileDropped.emit(files);
        }
    }
}
DragDropFilesDirective.ɵfac = function DragDropFilesDirective_Factory(t) { return new (t || DragDropFilesDirective)(); };
DragDropFilesDirective.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: DragDropFilesDirective, selectors: [["", "clickDragDropFiles", ""]], hostVars: 2, hostBindings: function DragDropFilesDirective_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("dragover", function DragDropFilesDirective_dragover_HostBindingHandler($event) { return ctx.dragOver($event); })("dragleave", function DragDropFilesDirective_dragleave_HostBindingHandler($event) { return ctx.dragLeave($event); })("drop", function DragDropFilesDirective_drop_HostBindingHandler($event) { return ctx.drop($event); });
    } if (rf & 2) {
        i0.ɵɵclassMap(ctx.cssClass);
    } }, outputs: { fileDropped: "fileDropped" } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DragDropFilesDirective, [{
        type: Directive,
        args: [{
                selector: '[clickDragDropFiles]'
            }]
    }], null, { fileDropped: [{
            type: Output
        }], cssClass: [{
            type: HostBinding,
            args: ['class']
        }], dragOver: [{
            type: HostListener,
            args: ['dragover', ['$event']]
        }], dragLeave: [{
            type: HostListener,
            args: ['dragleave', ['$event']]
        }], drop: [{
            type: HostListener,
            args: ['drop', ['$event']]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJhZy1kcm9wLWZpbGVzLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvZGlyZWN0aXZlcy9kcmFnLWRyb3AtZmlsZXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQWMsWUFBWSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQVMsTUFBTSxFQUFZLE1BQU0sZUFBZSxDQUFDOztBQU12SCxNQUFNLE9BQU8sc0JBQXNCO0lBSG5DO1FBS1ksZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBRWxCLGFBQVEsR0FBRyx1QkFBdUIsQ0FBQztLQTBCbEU7SUF4QkMsaUJBQWlCO0lBQ3FCLFFBQVEsQ0FBQyxLQUFLO1FBQ2xELEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxrREFBa0QsQ0FBQztJQUNyRSxDQUFDO0lBRUQsa0JBQWtCO0lBQzRCLFNBQVMsQ0FBQyxLQUFLO1FBQzNELEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxtREFBbUQsQ0FBQztJQUN0RSxDQUFDO0lBRUQsYUFBYTtJQUM0QixJQUFJLENBQUMsS0FBSztRQUNqRCxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEdBQUcsa0RBQWtELENBQUM7UUFDbkUsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7OzRGQTdCVSxzQkFBc0I7eUVBQXRCLHNCQUFzQjsrR0FBdEIsb0JBQWdCLGdHQUFoQixxQkFBaUIsc0ZBQWpCLGdCQUFZOzs7O3VGQUFaLHNCQUFzQjtjQUhsQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjthQUNqQztnQkFHVyxXQUFXO2tCQUFwQixNQUFNO1lBRXVCLFFBQVE7a0JBQXJDLFdBQVc7bUJBQUMsT0FBTztZQUdrQixRQUFRO2tCQUE3QyxZQUFZO21CQUFDLFVBQVUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQU9VLFNBQVM7a0JBQXRELFlBQVk7bUJBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDO1lBT0ksSUFBSTtrQkFBNUMsWUFBWTttQkFBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0RpcmVjdGl2ZSwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBIb3N0QmluZGluZywgSG9zdExpc3RlbmVyLCBJbnB1dCwgT3V0cHV0LCBSZW5kZXJlcjJ9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5ARGlyZWN0aXZlKHtcbiAgc2VsZWN0b3I6ICdbY2xpY2tEcmFnRHJvcEZpbGVzXSdcbn0pXG5leHBvcnQgY2xhc3MgRHJhZ0Ryb3BGaWxlc0RpcmVjdGl2ZSB7XG5cbiAgQE91dHB1dCgpIGZpbGVEcm9wcGVkID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG5cbiAgQEhvc3RCaW5kaW5nKCdjbGFzcycpIHByaXZhdGUgY3NzQ2xhc3MgPSAnY2xpY2stZHJhZy1kcm9wLWZpbGVzJztcblxuICAvLyBEcmFnb3ZlciBFdmVudFxuICBASG9zdExpc3RlbmVyKCdkcmFnb3ZlcicsIFsnJGV2ZW50J10pIGRyYWdPdmVyKGV2ZW50KSB7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICB0aGlzLmNzc0NsYXNzID0gJ2NsaWNrLWRyYWctZHJvcC1maWxlcyBjbGljay1kcmFnLWRyb3AtZmlsZXMtb3Zlcic7XG4gIH1cblxuICAvLyBEcmFnbGVhdmUgRXZlbnRcbiAgQEhvc3RMaXN0ZW5lcignZHJhZ2xlYXZlJywgWyckZXZlbnQnXSkgcHVibGljIGRyYWdMZWF2ZShldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgdGhpcy5jc3NDbGFzcyA9ICdjbGljay1kcmFnLWRyb3AtZmlsZXMgY2xpY2stZHJhZy1kcm9wLWZpbGVzLWxlYXZlJztcbiAgfVxuXG4gIC8vIERyb3AgRXZlbnRcbiAgQEhvc3RMaXN0ZW5lcignZHJvcCcsIFsnJGV2ZW50J10pIHB1YmxpYyBkcm9wKGV2ZW50KSB7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICB0aGlzLmNzc0NsYXNzID0gJ2NsaWNrLWRyYWctZHJvcC1maWxlcyBjbGljay1kcmFnLWRyb3AtZmlsZXMtZHJvcCc7XG4gICAgY29uc3QgZmlsZXMgPSBldmVudC5kYXRhVHJhbnNmZXIuZmlsZXM7XG4gICAgaWYgKGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuZmlsZURyb3BwZWQuZW1pdChmaWxlcyk7XG4gICAgfVxuICB9XG59XG4iXX0=