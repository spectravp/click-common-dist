import { NgModule, InjectionToken } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ClickModuleOptions } from './models/click-module-options.model';
import { ApiService } from './services/api.service';
import { ApiErrorComponent } from './components/api-error/api-error.component';
import { Hash } from './utils/hash';
import { AuthorizationInterceptor } from './interceptors/authorization.interceptor';
import { CommonModule } from '@angular/common';
import { StringifyImageDirective } from './directives/stringify-image.directive';
import { MongoDatePipe } from './pipes/mongo-date.pipe';
import { DragDropFilesDirective } from './directives/drag-drop-files.directive';
import * as i0 from "@angular/core";
export { ClickModuleOptions };
export { ApiService };
export { Hash };
export class ClickModule {
    // I setup the module providers for the root application.
    static forRoot(options) {
        return ({
            ngModule: ClickModule,
            providers: [
                // In order to translate the raw, optional OPTIONS argument into an
                // instance of the MyServiceOptions TYPE, we have to first provide it as
                // an injectable so that we can inject it into our FACTORY FUNCTION.
                {
                    provide: FOR_ROOT_OPTIONS_TOKEN,
                    useValue: options
                },
                // Once we've provided the OPTIONS as an injectable, we can use a FACTORY
                // FUNCTION to then take that raw configuration object and use it to
                // configure an instance of the MyServiceOptions TYPE (which will be
                // implicitly consumed by the MyService constructor).
                {
                    provide: ClickModuleOptions,
                    useFactory: provideApiServiceOptions,
                    deps: [FOR_ROOT_OPTIONS_TOKEN]
                }
                // NOTE: We don't have to explicitly provide the MyService class here
                // since it will automatically be picked-up using the "providedIn"
                // Injectable() meta-data.
            ]
        });
    }
}
ClickModule.ɵfac = function ClickModule_Factory(t) { return new (t || ClickModule)(); };
ClickModule.ɵmod = /*@__PURE__*/ i0.ɵɵdefineNgModule({ type: ClickModule });
ClickModule.ɵinj = /*@__PURE__*/ i0.ɵɵdefineInjector({ providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthorizationInterceptor,
            multi: true
        }
    ], imports: [CommonModule,
        HttpClientModule] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ClickModule, [{
        type: NgModule,
        args: [{
                declarations: [ApiErrorComponent, StringifyImageDirective, MongoDatePipe, DragDropFilesDirective],
                imports: [
                    CommonModule,
                    HttpClientModule,
                ],
                providers: [
                    {
                        provide: HTTP_INTERCEPTORS,
                        useClass: AuthorizationInterceptor,
                        multi: true
                    }
                ],
                exports: [
                    ApiErrorComponent,
                    StringifyImageDirective,
                    MongoDatePipe,
                    DragDropFilesDirective
                ]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ClickModule, { declarations: [ApiErrorComponent, StringifyImageDirective, MongoDatePipe, DragDropFilesDirective], imports: [CommonModule,
        HttpClientModule], exports: [ApiErrorComponent,
        StringifyImageDirective,
        MongoDatePipe,
        DragDropFilesDirective] }); })();
// I am the token that makes the raw options available to the following factory function.
// --
// NOTE: This value has to be exported otherwise the AoT compiler won't see it.
export const FOR_ROOT_OPTIONS_TOKEN = new InjectionToken('forRoot() ApiService configuration.');
// I translate the given raw OPTIONS into an instance of the MyServiceOptions TYPE. This
// will allows the MyService class to be instantiated and injected with a fully-formed
// configuration class instead of having to deal with the Inject() meta-data and a half-
// baked set of configuration options.
// --
// NOTE: This value has to be exported otherwise the AoT compiler won't see it.
export function provideApiServiceOptions(options) {
    // If the optional options were provided via the .forRoot() static method, then apply
    // them to the MyServiceOptions Type provider.
    if (options) {
        if (typeof (options.apiUrl) === 'string') {
            ClickModuleOptions.apiUrl = options.apiUrl;
        }
        if (typeof (options.logging) !== 'undefined') {
            ClickModuleOptions.logging = options.logging;
        }
        if (typeof (options.loginRoute) !== 'undefined') {
            ClickModuleOptions.loginRoute = options.loginRoute;
        }
        if (typeof (options.apiSecretKey) !== 'undefined') {
            ClickModuleOptions.apiSecretKey = options.apiSecretKey;
        }
        ClickModuleOptions.lock();
    }
    return (ClickModuleOptions);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpY2subW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vcHJvamVjdHMvY2xpY2svc3JjL2xpYi9jbGljay5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFzQixRQUFRLEVBQUUsY0FBYyxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBQyxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBQyxNQUFNLHNCQUFzQixDQUFDO0FBQ3pFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxNQUFNLHFDQUFxQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSx3QkFBd0IsQ0FBQztBQUNsRCxPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSw0Q0FBNEMsQ0FBQztBQUM3RSxPQUFPLEVBQUMsSUFBSSxFQUFDLE1BQU0sY0FBYyxDQUFDO0FBQ2xDLE9BQU8sRUFBQyx3QkFBd0IsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQ2xGLE9BQU8sRUFBQyxZQUFZLEVBQUMsTUFBTSxpQkFBaUIsQ0FBQztBQUM3QyxPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSx3Q0FBd0MsQ0FBQztBQUMvRSxPQUFPLEVBQUMsYUFBYSxFQUFDLE1BQU0seUJBQXlCLENBQUM7QUFDdEQsT0FBTyxFQUFDLHNCQUFzQixFQUFDLE1BQU0sd0NBQXdDLENBQUM7O0FBRTlFLE9BQU8sRUFBQyxrQkFBa0IsRUFBQyxDQUFDO0FBQzVCLE9BQU8sRUFBQyxVQUFVLEVBQUMsQ0FBQztBQUNwQixPQUFPLEVBQUMsSUFBSSxFQUFDLENBQUM7QUFzQmQsTUFBTSxPQUFPLFdBQVc7SUFDdEIseURBQXlEO0lBQ3pELE1BQU0sQ0FBQyxPQUFPLENBQUUsT0FBdUI7UUFFckMsT0FBTSxDQUFDO1lBQ0wsUUFBUSxFQUFFLFdBQVc7WUFDckIsU0FBUyxFQUFFO2dCQUNULG1FQUFtRTtnQkFDbkUsd0VBQXdFO2dCQUN4RSxvRUFBb0U7Z0JBQ3BFO29CQUNFLE9BQU8sRUFBRSxzQkFBc0I7b0JBQy9CLFFBQVEsRUFBRSxPQUFPO2lCQUNsQjtnQkFDRCx5RUFBeUU7Z0JBQ3pFLG9FQUFvRTtnQkFDcEUsb0VBQW9FO2dCQUNwRSxxREFBcUQ7Z0JBQ3JEO29CQUNFLE9BQU8sRUFBRSxrQkFBa0I7b0JBQzNCLFVBQVUsRUFBRSx3QkFBd0I7b0JBQ3BDLElBQUksRUFBRSxDQUFFLHNCQUFzQixDQUFFO2lCQUNqQztnQkFFRCxxRUFBcUU7Z0JBQ3JFLGtFQUFrRTtnQkFDbEUsMEJBQTBCO2FBQzNCO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7c0VBN0JVLFdBQVc7NkRBQVgsV0FBVztrRUFkWDtRQUNUO1lBQ0UsT0FBTyxFQUFFLGlCQUFpQjtZQUMxQixRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLEtBQUssRUFBRSxJQUFJO1NBQ1o7S0FDRixZQVRDLFlBQVk7UUFDWixnQkFBZ0I7dUZBZ0JQLFdBQVc7Y0FwQnZCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSx1QkFBdUIsRUFBRSxhQUFhLEVBQUUsc0JBQXNCLENBQUM7Z0JBQ2pHLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGdCQUFnQjtpQkFDakI7Z0JBQ0QsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFFBQVEsRUFBRSx3QkFBd0I7d0JBQ2xDLEtBQUssRUFBRSxJQUFJO3FCQUNaO2lCQUNGO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxpQkFBaUI7b0JBQ2pCLHVCQUF1QjtvQkFDdkIsYUFBYTtvQkFDYixzQkFBc0I7aUJBQ3ZCO2FBQ0Y7O3dGQUNZLFdBQVcsbUJBbkJQLGlCQUFpQixFQUFFLHVCQUF1QixFQUFFLGFBQWEsRUFBRSxzQkFBc0IsYUFFOUYsWUFBWTtRQUNaLGdCQUFnQixhQVVoQixpQkFBaUI7UUFDakIsdUJBQXVCO1FBQ3ZCLGFBQWE7UUFDYixzQkFBc0I7QUEyQzFCLHlGQUF5RjtBQUN6RixLQUFLO0FBQ0wsK0VBQStFO0FBQy9FLE1BQU0sQ0FBQyxNQUFNLHNCQUFzQixHQUFHLElBQUksY0FBYyxDQUFpQixxQ0FBcUMsQ0FBRSxDQUFDO0FBRWpILHdGQUF3RjtBQUN4RixzRkFBc0Y7QUFDdEYsd0ZBQXdGO0FBQ3hGLHNDQUFzQztBQUN0QyxLQUFLO0FBQ0wsK0VBQStFO0FBQy9FLE1BQU0sVUFBVSx3QkFBd0IsQ0FBRSxPQUF1QjtJQUUvRCxxRkFBcUY7SUFDckYsOENBQThDO0lBQzlDLElBQUssT0FBTyxFQUFHO1FBQ2IsSUFBSyxPQUFNLENBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBRSxLQUFLLFFBQVEsRUFBRztZQUMzQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQztTQUM1QztRQUVELElBQUssT0FBTSxDQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUUsS0FBSyxXQUFXLEVBQUc7WUFDL0Msa0JBQWtCLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUM7U0FDOUM7UUFFRCxJQUFLLE9BQU0sQ0FBRSxPQUFPLENBQUMsVUFBVSxDQUFFLEtBQUssV0FBVyxFQUFHO1lBQ2xELGtCQUFrQixDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO1NBQ3BEO1FBRUQsSUFBSyxPQUFNLENBQUUsT0FBTyxDQUFDLFlBQVksQ0FBRSxLQUFLLFdBQVcsRUFBRztZQUNwRCxrQkFBa0IsQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztTQUN4RDtRQUVELGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0tBQzNCO0lBRUQsT0FBTSxDQUFFLGtCQUFrQixDQUFFLENBQUM7QUFDL0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIEluamVjdGlvblRva2VufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7SFRUUF9JTlRFUkNFUFRPUlMsIEh0dHBDbGllbnRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7Q2xpY2tNb2R1bGVPcHRpb25zfSBmcm9tICcuL21vZGVscy9jbGljay1tb2R1bGUtb3B0aW9ucy5tb2RlbCc7XG5pbXBvcnQge0FwaVNlcnZpY2V9IGZyb20gJy4vc2VydmljZXMvYXBpLnNlcnZpY2UnO1xuaW1wb3J0IHtBcGlFcnJvckNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2FwaS1lcnJvci9hcGktZXJyb3IuY29tcG9uZW50JztcbmltcG9ydCB7SGFzaH0gZnJvbSAnLi91dGlscy9oYXNoJztcbmltcG9ydCB7QXV0aG9yaXphdGlvbkludGVyY2VwdG9yfSBmcm9tICcuL2ludGVyY2VwdG9ycy9hdXRob3JpemF0aW9uLmludGVyY2VwdG9yJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtTdHJpbmdpZnlJbWFnZURpcmVjdGl2ZX0gZnJvbSAnLi9kaXJlY3RpdmVzL3N0cmluZ2lmeS1pbWFnZS5kaXJlY3RpdmUnO1xuaW1wb3J0IHtNb25nb0RhdGVQaXBlfSBmcm9tICcuL3BpcGVzL21vbmdvLWRhdGUucGlwZSc7XG5pbXBvcnQge0RyYWdEcm9wRmlsZXNEaXJlY3RpdmV9IGZyb20gJy4vZGlyZWN0aXZlcy9kcmFnLWRyb3AtZmlsZXMuZGlyZWN0aXZlJztcblxuZXhwb3J0IHtDbGlja01vZHVsZU9wdGlvbnN9O1xuZXhwb3J0IHtBcGlTZXJ2aWNlfTtcbmV4cG9ydCB7SGFzaH07XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0FwaUVycm9yQ29tcG9uZW50LCBTdHJpbmdpZnlJbWFnZURpcmVjdGl2ZSwgTW9uZ29EYXRlUGlwZSwgRHJhZ0Ryb3BGaWxlc0RpcmVjdGl2ZV0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgSHR0cENsaWVudE1vZHVsZSxcbiAgXSxcbiAgcHJvdmlkZXJzOiBbXG4gICAge1xuICAgICAgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsXG4gICAgICB1c2VDbGFzczogQXV0aG9yaXphdGlvbkludGVyY2VwdG9yLFxuICAgICAgbXVsdGk6IHRydWVcbiAgICB9XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBcGlFcnJvckNvbXBvbmVudCxcbiAgICBTdHJpbmdpZnlJbWFnZURpcmVjdGl2ZSxcbiAgICBNb25nb0RhdGVQaXBlLFxuICAgIERyYWdEcm9wRmlsZXNEaXJlY3RpdmVcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBDbGlja01vZHVsZSB7XG4gIC8vIEkgc2V0dXAgdGhlIG1vZHVsZSBwcm92aWRlcnMgZm9yIHRoZSByb290IGFwcGxpY2F0aW9uLlxuICBzdGF0aWMgZm9yUm9vdCggb3B0aW9ucz86IE1vZHVsZU9wdGlvbnMgKTogTW9kdWxlV2l0aFByb3ZpZGVyczxDbGlja01vZHVsZT4ge1xuXG4gICAgcmV0dXJuKHtcbiAgICAgIG5nTW9kdWxlOiBDbGlja01vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAvLyBJbiBvcmRlciB0byB0cmFuc2xhdGUgdGhlIHJhdywgb3B0aW9uYWwgT1BUSU9OUyBhcmd1bWVudCBpbnRvIGFuXG4gICAgICAgIC8vIGluc3RhbmNlIG9mIHRoZSBNeVNlcnZpY2VPcHRpb25zIFRZUEUsIHdlIGhhdmUgdG8gZmlyc3QgcHJvdmlkZSBpdCBhc1xuICAgICAgICAvLyBhbiBpbmplY3RhYmxlIHNvIHRoYXQgd2UgY2FuIGluamVjdCBpdCBpbnRvIG91ciBGQUNUT1JZIEZVTkNUSU9OLlxuICAgICAgICB7XG4gICAgICAgICAgcHJvdmlkZTogRk9SX1JPT1RfT1BUSU9OU19UT0tFTixcbiAgICAgICAgICB1c2VWYWx1ZTogb3B0aW9uc1xuICAgICAgICB9LFxuICAgICAgICAvLyBPbmNlIHdlJ3ZlIHByb3ZpZGVkIHRoZSBPUFRJT05TIGFzIGFuIGluamVjdGFibGUsIHdlIGNhbiB1c2UgYSBGQUNUT1JZXG4gICAgICAgIC8vIEZVTkNUSU9OIHRvIHRoZW4gdGFrZSB0aGF0IHJhdyBjb25maWd1cmF0aW9uIG9iamVjdCBhbmQgdXNlIGl0IHRvXG4gICAgICAgIC8vIGNvbmZpZ3VyZSBhbiBpbnN0YW5jZSBvZiB0aGUgTXlTZXJ2aWNlT3B0aW9ucyBUWVBFICh3aGljaCB3aWxsIGJlXG4gICAgICAgIC8vIGltcGxpY2l0bHkgY29uc3VtZWQgYnkgdGhlIE15U2VydmljZSBjb25zdHJ1Y3RvcikuXG4gICAgICAgIHtcbiAgICAgICAgICBwcm92aWRlOiBDbGlja01vZHVsZU9wdGlvbnMsXG4gICAgICAgICAgdXNlRmFjdG9yeTogcHJvdmlkZUFwaVNlcnZpY2VPcHRpb25zLFxuICAgICAgICAgIGRlcHM6IFsgRk9SX1JPT1RfT1BUSU9OU19UT0tFTiBdXG4gICAgICAgIH1cblxuICAgICAgICAvLyBOT1RFOiBXZSBkb24ndCBoYXZlIHRvIGV4cGxpY2l0bHkgcHJvdmlkZSB0aGUgTXlTZXJ2aWNlIGNsYXNzIGhlcmVcbiAgICAgICAgLy8gc2luY2UgaXQgd2lsbCBhdXRvbWF0aWNhbGx5IGJlIHBpY2tlZC11cCB1c2luZyB0aGUgXCJwcm92aWRlZEluXCJcbiAgICAgICAgLy8gSW5qZWN0YWJsZSgpIG1ldGEtZGF0YS5cbiAgICAgIF1cbiAgICB9KTtcbiAgfVxufVxuXG4vLyBJIGRlZmluZSB0aGUgc2hhcGUgb2YgdGhlIG9wdGlvbmFsIGNvbmZpZ3VyYXRpb24gZGF0YSBwYXNzZWQgdG8gdGhlIGZvclJvb3QoKSBtZXRob2QuXG5leHBvcnQgaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnMge1xuICBhcGlVcmw/OiBzdHJpbmc7XG4gIGxvZ2dpbmc/OiBib29sZWFuO1xuICBsb2dpblJvdXRlPzogc3RyaW5nO1xuICBhcGlTZWNyZXRLZXk/OiBzdHJpbmc7XG59XG5cbi8vIEkgYW0gdGhlIHRva2VuIHRoYXQgbWFrZXMgdGhlIHJhdyBvcHRpb25zIGF2YWlsYWJsZSB0byB0aGUgZm9sbG93aW5nIGZhY3RvcnkgZnVuY3Rpb24uXG4vLyAtLVxuLy8gTk9URTogVGhpcyB2YWx1ZSBoYXMgdG8gYmUgZXhwb3J0ZWQgb3RoZXJ3aXNlIHRoZSBBb1QgY29tcGlsZXIgd29uJ3Qgc2VlIGl0LlxuZXhwb3J0IGNvbnN0IEZPUl9ST09UX09QVElPTlNfVE9LRU4gPSBuZXcgSW5qZWN0aW9uVG9rZW48TW9kdWxlT3B0aW9ucz4oICdmb3JSb290KCkgQXBpU2VydmljZSBjb25maWd1cmF0aW9uLicgKTtcblxuLy8gSSB0cmFuc2xhdGUgdGhlIGdpdmVuIHJhdyBPUFRJT05TIGludG8gYW4gaW5zdGFuY2Ugb2YgdGhlIE15U2VydmljZU9wdGlvbnMgVFlQRS4gVGhpc1xuLy8gd2lsbCBhbGxvd3MgdGhlIE15U2VydmljZSBjbGFzcyB0byBiZSBpbnN0YW50aWF0ZWQgYW5kIGluamVjdGVkIHdpdGggYSBmdWxseS1mb3JtZWRcbi8vIGNvbmZpZ3VyYXRpb24gY2xhc3MgaW5zdGVhZCBvZiBoYXZpbmcgdG8gZGVhbCB3aXRoIHRoZSBJbmplY3QoKSBtZXRhLWRhdGEgYW5kIGEgaGFsZi1cbi8vIGJha2VkIHNldCBvZiBjb25maWd1cmF0aW9uIG9wdGlvbnMuXG4vLyAtLVxuLy8gTk9URTogVGhpcyB2YWx1ZSBoYXMgdG8gYmUgZXhwb3J0ZWQgb3RoZXJ3aXNlIHRoZSBBb1QgY29tcGlsZXIgd29uJ3Qgc2VlIGl0LlxuZXhwb3J0IGZ1bmN0aW9uIHByb3ZpZGVBcGlTZXJ2aWNlT3B0aW9ucyggb3B0aW9ucz86IE1vZHVsZU9wdGlvbnMgKTogQ2xpY2tNb2R1bGVPcHRpb25zIHtcblxuICAvLyBJZiB0aGUgb3B0aW9uYWwgb3B0aW9ucyB3ZXJlIHByb3ZpZGVkIHZpYSB0aGUgLmZvclJvb3QoKSBzdGF0aWMgbWV0aG9kLCB0aGVuIGFwcGx5XG4gIC8vIHRoZW0gdG8gdGhlIE15U2VydmljZU9wdGlvbnMgVHlwZSBwcm92aWRlci5cbiAgaWYgKCBvcHRpb25zICkge1xuICAgIGlmICggdHlwZW9mKCBvcHRpb25zLmFwaVVybCApID09PSAnc3RyaW5nJyApIHtcbiAgICAgIENsaWNrTW9kdWxlT3B0aW9ucy5hcGlVcmwgPSBvcHRpb25zLmFwaVVybDtcbiAgICB9XG5cbiAgICBpZiAoIHR5cGVvZiggb3B0aW9ucy5sb2dnaW5nICkgIT09ICd1bmRlZmluZWQnICkge1xuICAgICAgQ2xpY2tNb2R1bGVPcHRpb25zLmxvZ2dpbmcgPSBvcHRpb25zLmxvZ2dpbmc7XG4gICAgfVxuXG4gICAgaWYgKCB0eXBlb2YoIG9wdGlvbnMubG9naW5Sb3V0ZSApICE9PSAndW5kZWZpbmVkJyApIHtcbiAgICAgIENsaWNrTW9kdWxlT3B0aW9ucy5sb2dpblJvdXRlID0gb3B0aW9ucy5sb2dpblJvdXRlO1xuICAgIH1cblxuICAgIGlmICggdHlwZW9mKCBvcHRpb25zLmFwaVNlY3JldEtleSApICE9PSAndW5kZWZpbmVkJyApIHtcbiAgICAgIENsaWNrTW9kdWxlT3B0aW9ucy5hcGlTZWNyZXRLZXkgPSBvcHRpb25zLmFwaVNlY3JldEtleTtcbiAgICB9XG5cbiAgICBDbGlja01vZHVsZU9wdGlvbnMubG9jaygpO1xuICB9XG5cbiAgcmV0dXJuKCBDbGlja01vZHVsZU9wdGlvbnMgKTtcbn1cbiJdfQ==