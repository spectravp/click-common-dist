import { Injectable } from '@angular/core';
import { AuthenticatedUserModel } from '../models/authenticated-user.model';
import { ClickModuleOptions } from '../models/click-module-options.model';
import * as i0 from "@angular/core";
export class AuthorizationInterceptor {
    /**
     * Constructor
     */
    constructor() { }
    /**
     * Interceptor to add the Authorization header if we have a valid user
     */
    intercept(request, next) {
        console.group('AuthorizationInterceptor');
        const user = new AuthenticatedUserModel();
        if (user.isValid() && request.url.indexOf(ClickModuleOptions.apiUrl) >= 0) {
            console.log('Adding header', user.accessToken);
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${user.accessToken}`
                }
            });
        }
        else {
            if (user.isValid()) {
                console.log('Not adding header because the user is not valid');
            }
            if (request.url.indexOf(ClickModuleOptions.apiUrl) < 0) {
                console.log('Not adding header because its not our API');
            }
        }
        console.groupEnd();
        return next.handle(request);
    }
}
AuthorizationInterceptor.ɵfac = function AuthorizationInterceptor_Factory(t) { return new (t || AuthorizationInterceptor)(); };
AuthorizationInterceptor.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthorizationInterceptor, factory: AuthorizationInterceptor.ɵfac });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthorizationInterceptor, [{
        type: Injectable
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aG9yaXphdGlvbi5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2NsaWNrL3NyYy9saWIvaW50ZXJjZXB0b3JzL2F1dGhvcml6YXRpb24uaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUUxRSxPQUFPLEVBQUMsa0JBQWtCLEVBQUMsTUFBTSxzQ0FBc0MsQ0FBQzs7QUFHeEUsTUFBTSxPQUFPLHdCQUF3QjtJQUVuQzs7T0FFRztJQUNILGdCQUFlLENBQUM7SUFFaEI7O09BRUc7SUFDSCxTQUFTLENBQUMsT0FBeUIsRUFBRSxJQUFpQjtRQUNwRCxPQUFPLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDMUMsTUFBTSxJQUFJLEdBQUcsSUFBSSxzQkFBc0IsRUFBRSxDQUFDO1FBQzFDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN6RSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDL0MsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ3RCLFVBQVUsRUFBRTtvQkFDVixhQUFhLEVBQUUsVUFBVSxJQUFJLENBQUMsV0FBVyxFQUFFO2lCQUM1QzthQUNGLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpREFBaUQsQ0FBQyxDQUFDO2FBQ2hFO1lBQ0QsSUFBSSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsQ0FBQzthQUMxRDtTQUNGO1FBRUQsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ25CLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM5QixDQUFDOztnR0EvQlUsd0JBQXdCOzhFQUF4Qix3QkFBd0IsV0FBeEIsd0JBQXdCO3VGQUF4Qix3QkFBd0I7Y0FEcEMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0h0dHBSZXF1ZXN0LCBIdHRwSGFuZGxlciwgSHR0cEV2ZW50LCBIdHRwSW50ZXJjZXB0b3J9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7QXV0aGVudGljYXRlZFVzZXJNb2RlbH0gZnJvbSAnLi4vbW9kZWxzL2F1dGhlbnRpY2F0ZWQtdXNlci5tb2RlbCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtDbGlja01vZHVsZU9wdGlvbnN9IGZyb20gJy4uL21vZGVscy9jbGljay1tb2R1bGUtb3B0aW9ucy5tb2RlbCc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBdXRob3JpemF0aW9uSW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xuXG4gIC8qKlxuICAgKiBDb25zdHJ1Y3RvclxuICAgKi9cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIC8qKlxuICAgKiBJbnRlcmNlcHRvciB0byBhZGQgdGhlIEF1dGhvcml6YXRpb24gaGVhZGVyIGlmIHdlIGhhdmUgYSB2YWxpZCB1c2VyXG4gICAqL1xuICBpbnRlcmNlcHQocmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PiwgbmV4dDogSHR0cEhhbmRsZXIpOiBPYnNlcnZhYmxlPEh0dHBFdmVudDxhbnk+PiB7XG4gICAgY29uc29sZS5ncm91cCgnQXV0aG9yaXphdGlvbkludGVyY2VwdG9yJyk7XG4gICAgY29uc3QgdXNlciA9IG5ldyBBdXRoZW50aWNhdGVkVXNlck1vZGVsKCk7XG4gICAgaWYgKHVzZXIuaXNWYWxpZCgpICYmIHJlcXVlc3QudXJsLmluZGV4T2YoQ2xpY2tNb2R1bGVPcHRpb25zLmFwaVVybCkgPj0gMCkge1xuICAgICAgY29uc29sZS5sb2coJ0FkZGluZyBoZWFkZXInLCB1c2VyLmFjY2Vzc1Rva2VuKTtcbiAgICAgIHJlcXVlc3QgPSByZXF1ZXN0LmNsb25lKHtcbiAgICAgICAgc2V0SGVhZGVyczoge1xuICAgICAgICAgIEF1dGhvcml6YXRpb246IGBCZWFyZXIgJHt1c2VyLmFjY2Vzc1Rva2VufWBcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmICh1c2VyLmlzVmFsaWQoKSkge1xuICAgICAgICBjb25zb2xlLmxvZygnTm90IGFkZGluZyBoZWFkZXIgYmVjYXVzZSB0aGUgdXNlciBpcyBub3QgdmFsaWQnKTtcbiAgICAgIH1cbiAgICAgIGlmIChyZXF1ZXN0LnVybC5pbmRleE9mKENsaWNrTW9kdWxlT3B0aW9ucy5hcGlVcmwpIDwgMCkge1xuICAgICAgICBjb25zb2xlLmxvZygnTm90IGFkZGluZyBoZWFkZXIgYmVjYXVzZSBpdHMgbm90IG91ciBBUEknKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zb2xlLmdyb3VwRW5kKCk7XG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpO1xuICB9XG59XG4iXX0=