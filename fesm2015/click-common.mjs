import * as i0 from '@angular/core';
import { Injectable, Component, Input, EventEmitter, Directive, Output, HostListener, Pipe, HostBinding, NgModule, InjectionToken } from '@angular/core';
import * as i2 from '@angular/common/http';
import { HttpRequest, HttpHeaders, HttpErrorResponse, HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import * as i1 from '@angular/common';
import { CommonModule } from '@angular/common';
import CryptoES from 'crypto-es';
import { __awaiter } from 'tslib';
import { UntypedFormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import * as i1$1 from '@angular/router';

class ClickModuleOptions {
    static setOptions(options) {
        if (options) {
            for (const prop in options) {
                ClickModuleOptions[prop] = options[prop];
            }
        }
    }
    static get apiUrl() {
        return this._apiUrl;
    }
    static set apiUrl(value) {
        if (!ClickModuleOptions._locked) {
            this._apiUrl = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get logging() {
        return this._logging;
    }
    static set logging(value) {
        if (!ClickModuleOptions._locked) {
            this._logging = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get loginRoute() {
        return this._loginRoute;
    }
    static set loginRoute(value) {
        if (!ClickModuleOptions._locked) {
            this._loginRoute = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static get apiSecretKey() {
        return this._apiSecretKey;
    }
    static set apiSecretKey(value) {
        if (!ClickModuleOptions._locked) {
            this._apiSecretKey = value;
        }
        else {
            throw new Error('Click Module Options have been initialized and locked. You cannot change them now');
        }
    }
    static lock() {
        ClickModuleOptions._locked = true;
    }
}
ClickModuleOptions._apiUrl = '/api/';
ClickModuleOptions._logging = true;
ClickModuleOptions._loginRoute = '/login';
ClickModuleOptions._locked = false;
ClickModuleOptions.ɵfac = function ClickModuleOptions_Factory(t) { return new (t || ClickModuleOptions)(); };
ClickModuleOptions.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ClickModuleOptions, factory: ClickModuleOptions.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ClickModuleOptions, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], null, null);
})();

class ApiRequestModel {
    constructor() {
        this.method = 'GET';
        this.body = null;
        this.params = null;
        this.headers = null;
    }
}

class ApiService {
    /**
     * Constructor
     */
    constructor(options, httpClient) {
        this.options = options;
        this.httpClient = httpClient;
    }
    /**
     * Makes an API call
     * You can pass in individual variables, or you can use an
     * object that conforms to the ApiRequestModel interface:
     * {url: '/somethinbg'}
     * {url: '/something, method: ApiService.POST, body: {name: 'test'}}
     */
    call(url, method = 'GET', body = null, params = null, headers = null) {
        switch (method.toUpperCase()) {
            case ApiService.GET: {
                return this.get(url, params, headers);
            }
            case ApiService.POST: {
                return this.post(url, body, params, headers);
            }
            case ApiService.PUT: {
                return this.put(url, body, params, headers);
            }
            case ApiService.PATCH: {
                return this.patch(url, body, params, headers);
            }
            case ApiService.DELETE: {
                return this.delete(url, params, headers);
            }
        }
    }
    /**
     * Executes a GET
     */
    get(url, params = null, headers = null) {
        const request = this.buildRequest(url, ApiService.GET, null, params, headers);
        return this.httpClient.get(request.url, { headers: request.headers, params: request.params }).toPromise();
    }
    /**
     * Executes a POST
     */
    post(url, body = null, params = null, headers = null) {
        const request = this.buildRequest(url, ApiService.POST, body, params, headers);
        return this.httpClient.post(request.url, request.body, { headers: request.headers, params: request.params }).toPromise();
    }
    /**
     * POST as form data
     */
    postFormData(url, body = null) {
        const request = this.buildRequest(url, ApiService.POST, body);
        return this.httpClient.post(request.url, request.body).toPromise();
    }
    /**
     * Executes a PUT
     */
    put(url, body = null, params = null, headers = null) {
        const request = this.buildRequest(url, ApiService.PUT, body, params, headers);
        return this.httpClient.put(request.url, request.body, { headers: request.headers, params: request.params }).toPromise();
    }
    /**
     * PUT as form data
     */
    putFormData(url, body = null) {
        const request = this.buildRequest(url, ApiService.PUT, body);
        return this.httpClient.put(request.url, request.body).toPromise();
    }
    /**
     * Executes a PATCH
     */
    patch(url, body = null, params = null, headers = null) {
        const request = this.buildRequest(url, ApiService.PATCH, body, params, headers);
        return this.httpClient.patch(request.url, request.body, { headers: request.headers, params: request.params }).toPromise();
    }
    /**
     * Executes a DELETE
     */
    delete(url, params = null, headers = null) {
        const request = this.buildRequest(url, ApiService.DELETE, null, params, headers);
        return this.httpClient.delete(request.url, { headers: request.headers, params: request.params }).toPromise();
    }
    /**
     * Logs a request
     */
    logRequest(request) {
        if (ClickModuleOptions.logging) {
            console.group('ApiService');
            console.log('URL', request.url);
            console.log('Method', request.method);
            console.log('Data', request.body);
            console.log('Headers', request.headers.keys(), request.headers);
            console.log('Request', request);
            console.groupEnd();
        }
    }
    /**
     * Builds the request object
     */
    buildRequest(url, method = 'GET', body = null, params = null, headers = null) {
        if (url instanceof HttpRequest) {
            this.logRequest(url);
            return url;
        }
        if (url instanceof ApiRequestModel || typeof url !== 'string') {
            method = url.method;
            body = url.body;
            params = url.params;
            url = url.url;
        }
        if (url.indexOf('http:') < 0 && url.indexOf('https:') < 0) {
            url = ClickModuleOptions.apiUrl + url;
        }
        if (!headers) {
            headers = new HttpHeaders({
                'Content-Type': 'application/json',
                Accept: 'application/json',
            });
        }
        const request = new HttpRequest(method, url, body, { headers, params });
        this.logRequest(request);
        return request;
    }
}
ApiService.GET = 'GET';
ApiService.POST = 'POST';
ApiService.PUT = 'PUT';
ApiService.PATCH = 'PATCH';
ApiService.DELETE = 'DELETE';
ApiService.ɵfac = function ApiService_Factory(t) { return new (t || ApiService)(i0.ɵɵinject(ClickModuleOptions), i0.ɵɵinject(i2.HttpClient)); };
ApiService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ApiService, factory: ApiService.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ApiService, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: ClickModuleOptions }, { type: i2.HttpClient }]; }, null);
})();

function ApiErrorComponent_ul_0_li_1_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "li");
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const error_r2 = ctx.$implicit;
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(error_r2);
    }
}
function ApiErrorComponent_ul_0_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "ul", 1);
        i0.ɵɵtemplate(1, ApiErrorComponent_ul_0_li_1_Template, 2, 1, "li", 2);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r0 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx_r0.errors);
    }
}
class ApiErrorComponent {
    constructor() {
        this.prependAsterisk = false;
        this.errors = [];
    }
    ngOnChanges(changes) {
        this.errors.length = 0;
        if (!this.key) {
            console.error('No key set for API Error components');
            return;
        }
        if (this.apiErrors instanceof HttpErrorResponse && this.apiErrors.error && this.apiErrors.error.validation_messages) {
            if (this.apiErrors.error.validation_messages[this.key]) {
                for (const error in this.apiErrors.error.validation_messages[this.key]) {
                    let message = this.apiErrors.error.validation_messages[this.key][error];
                    if (this.prependAsterisk) {
                        message = '* ' + message;
                    }
                    this.errors.push(message);
                }
            }
        }
    }
}
ApiErrorComponent.ɵfac = function ApiErrorComponent_Factory(t) { return new (t || ApiErrorComponent)(); };
ApiErrorComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: ApiErrorComponent, selectors: [["click-api-error"]], inputs: { apiErrors: "apiErrors", key: "key", prependAsterisk: "prependAsterisk" }, features: [i0.ɵɵNgOnChangesFeature], decls: 1, vars: 1, consts: [["class", "click-api-errors", 4, "ngIf"], [1, "click-api-errors"], [4, "ngFor", "ngForOf"]], template: function ApiErrorComponent_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵtemplate(0, ApiErrorComponent_ul_0_Template, 2, 1, "ul", 0);
        }
        if (rf & 2) {
            i0.ɵɵproperty("ngIf", ctx.errors.length);
        }
    }, dependencies: [i1.NgForOf, i1.NgIf], styles: ["[_nghost-%COMP%]   ul[_ngcontent-%COMP%]{margin:0;padding:0;list-style:none;display:block}[_nghost-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]{color:#8b0000;font-size:.9rem;display:block}"] });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ApiErrorComponent, [{
            type: Component,
            args: [{ selector: 'click-api-error', template: "<ul *ngIf=\"errors.length\" class=\"click-api-errors\">\n  <li *ngFor=\"let error of errors\">{{error}}</li>\n</ul>\n", styles: [":host ul{margin:0;padding:0;list-style:none;display:block}:host ul li{color:#8b0000;font-size:.9rem;display:block}\n"] }]
        }], function () { return []; }, { apiErrors: [{
                type: Input
            }], key: [{
                type: Input
            }], prependAsterisk: [{
                type: Input
            }] });
})();

class Hash {
    /**
     * Generates a Unique Hash
     */
    static get() {
        const date = new Date();
        const timestamp = (date.getTime() / 1000).toString(16);
        const oid = timestamp + 'xxxxxxxxxxxxxxxx'
            .replace(/[x]/g, _ => (Math.random() * 16 | 0).toString(16))
            .toLowerCase();
        return oid.split('.').join('');
    }
}

class AuthenticatedUserEvent {
}
AuthenticatedUserEvent.AUTHENTICATED = 'AUTHENTICATED';
AuthenticatedUserEvent.DEAUTHENTICATED = 'DEAUTHENTICATED';

class AuthenticatedUserModel {
    /**
     * Builds the user object
     * @param apiResponse The raw api response from httpClient
     */
    constructor(apiResponse = null) {
        if (!ClickModuleOptions.apiSecretKey) {
            throw new Error('You have not set a secret key for the API. Make sure this is unique for each project and can be random');
        }
        if (!AuthenticatedUserModel.onChangesEventEmitter) {
            AuthenticatedUserModel.onChangesEventEmitter = new EventEmitter();
        }
        if (apiResponse !== null) {
            AuthenticatedUserModel.id = apiResponse.user_id;
            AuthenticatedUserModel.accessToken = apiResponse.access_token;
            AuthenticatedUserModel.expires = apiResponse.expires;
            AuthenticatedUserModel.expirationDate = new Date(apiResponse.expires * 1000);
            AuthenticatedUserModel.scope = apiResponse.scope;
            AuthenticatedUserModel.dataLoaded = true;
            window.localStorage.setItem(ClickModuleOptions.apiSecretKey, CryptoES.AES.encrypt(JSON.stringify(apiResponse), AuthenticatedUserModel.encryptSecretKey).toString());
            this.emitChanges(AuthenticatedUserEvent.AUTHENTICATED);
        }
        else if (AuthenticatedUserModel.dataLoaded === false) {
            const data = window.localStorage.getItem(ClickModuleOptions.apiSecretKey);
            if (data !== null) {
                try {
                    const bytes = CryptoES.AES.decrypt(data, AuthenticatedUserModel.encryptSecretKey);
                    if (bytes.toString()) {
                        const obj = JSON.parse(bytes.toString(CryptoES.enc.Utf8));
                        AuthenticatedUserModel.id = obj.user_id;
                        AuthenticatedUserModel.accessToken = obj.access_token;
                        AuthenticatedUserModel.expires = obj.expires;
                        AuthenticatedUserModel.expirationDate = new Date(obj.expires * 1000);
                        AuthenticatedUserModel.scope = obj.scope;
                        AuthenticatedUserModel.dataLoaded = true;
                        this.emitChanges(AuthenticatedUserEvent.AUTHENTICATED);
                    }
                }
                catch (e) {
                    console.error('Decryption Error', e);
                }
            }
        }
        // Listen for changes to the storage and test validity
        if (!AuthenticatedUserModel.storageEventListenerRegistered) {
            AuthenticatedUserModel.storageEventListenerRegistered = true;
            window.addEventListener('storage', () => {
                this.isValid();
            });
        }
    }
    /**
     * Destroys the user
     */
    destroyUser() {
        AuthenticatedUserModel.id = null;
        AuthenticatedUserModel.accessToken = null;
        AuthenticatedUserModel.expirationDate = null;
        AuthenticatedUserModel.expires = null;
        AuthenticatedUserModel.scope = null;
        AuthenticatedUserModel.dataLoaded = false;
        window.localStorage.removeItem(ClickModuleOptions.apiSecretKey);
        this.emitChanges(AuthenticatedUserEvent.DEAUTHENTICATED);
    }
    /**
     * Gets the users ID
     */
    get id() {
        return AuthenticatedUserModel.id;
    }
    /**
     * Gets the access token
     */
    get accessToken() {
        return AuthenticatedUserModel.accessToken;
    }
    /**
     * Gets the expiration Date
     */
    get expirationDate() {
        return AuthenticatedUserModel.expirationDate;
    }
    /**
     * Gets the scope
     */
    get scope() {
        return AuthenticatedUserModel.scope;
    }
    /**
     * Gets the raw expires value
     */
    get expires() {
        return AuthenticatedUserModel.expires;
    }
    /**
     * Tests if it has a valid user
     */
    isValid() {
        if (!AuthenticatedUserModel.accessToken || !AuthenticatedUserModel.expirationDate || !AuthenticatedUserModel.scope) {
            // We are missing critical information
            return false;
        }
        const now = new Date();
        if (now > AuthenticatedUserModel.expirationDate) {
            // The token has expired
            return false;
        }
        // If we don't have any keys in storage, we cannot be logged in or we are dead on refresh
        if (!window.localStorage.getItem(ClickModuleOptions.apiSecretKey)) {
            // we don't have anything in storage
            return false;
        }
        return true;
    }
    /**
     * Gets a copy as an object
     */
    getCopy() {
        return JSON.parse(this.getJson());
    }
    /**
     * Gets the data as a JSON string
     */
    getJson() {
        return JSON.stringify({
            id: AuthenticatedUserModel.id,
            accessToken: AuthenticatedUserModel.accessToken,
            expires: AuthenticatedUserModel.expires,
            expirationDate: AuthenticatedUserModel.expirationDate,
            scope: AuthenticatedUserModel.scope,
        });
    }
    /**
     * Gets the on changes event listener
     */
    get onChanges() {
        return AuthenticatedUserModel.onChangesEventEmitter;
    }
    emitChanges(type) {
        AuthenticatedUserModel.onChangesEventEmitter.emit({ type, model: this });
    }
}
/**
 * Flag for whether data has been loaded from local storage
 */
AuthenticatedUserModel.dataLoaded = false;
/**
 * The id of the user record
 */
AuthenticatedUserModel.id = null;
/**
 * The access token of the user
 */
AuthenticatedUserModel.accessToken = null;
/**
 * The expiration date of the user
 */
AuthenticatedUserModel.expirationDate = null;
/**
 * The raw expiration time
 */
AuthenticatedUserModel.expires = null;
/**
 * The scope of the token
 */
AuthenticatedUserModel.scope = null;
/**
 * Encryption string for storage
 */
AuthenticatedUserModel.encryptSecretKey = 'fhjsdg,javb.g7lfty4rqg,d';
/**
 * Storage Event registered flag
 */
AuthenticatedUserModel.storageEventListenerRegistered = false;

class AuthorizationInterceptor {
    /**
     * Constructor
     */
    constructor() { }
    /**
     * Interceptor to add the Authorization header if we have a valid user
     */
    intercept(request, next) {
        console.group('AuthorizationInterceptor');
        const user = new AuthenticatedUserModel();
        if (user.isValid() && request.url.indexOf(ClickModuleOptions.apiUrl) >= 0) {
            console.log('Adding header', user.accessToken);
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${user.accessToken}`
                }
            });
        }
        else {
            if (user.isValid()) {
                console.log('Not adding header because the user is not valid');
            }
            if (request.url.indexOf(ClickModuleOptions.apiUrl) < 0) {
                console.log('Not adding header because its not our API');
            }
        }
        console.groupEnd();
        return next.handle(request);
    }
}
AuthorizationInterceptor.ɵfac = function AuthorizationInterceptor_Factory(t) { return new (t || AuthorizationInterceptor)(); };
AuthorizationInterceptor.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthorizationInterceptor, factory: AuthorizationInterceptor.ɵfac });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthorizationInterceptor, [{
            type: Injectable
        }], function () { return []; }, null);
})();

class StringifyImageDirective {
    // constructor
    constructor(renderer, el) {
        this.renderer = renderer;
        this.el = el;
        // internal count of how many files were selected
        // used to determine if we need to be an array or not
        this.count = 0;
        // Setup an event emitter for changes to the model
        this.modelChange = new EventEmitter();
    }
    // Set up a change listener
    onChange(files) {
        if (files.length) {
            this.count = files.length;
            if (this.count > 1) {
                this.internalModel = [];
            }
            else {
                this.internalModel = '';
            }
            for (let i = 0; i < files.length; i++) {
                this.readFile(files[i]);
            }
        }
    }
    /**
     * Sets the model value and emits the change event
     */
    setModelValue(value) {
        if (this.internalModel instanceof Array) {
            this.internalModel.push(value);
        }
        else {
            this.internalModel = value;
        }
        if (this.model instanceof UntypedFormControl) {
            this.model.setValue(this.internalModel);
        }
        else {
            this.model = this.internalModel;
        }
        this.modelChange.emit(this.model);
    }
    /**
     * Reads the individual file
     */
    readFile(file) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = (e) => __awaiter(this, void 0, void 0, function* () {
            this.setModelValue(e.target.result);
        });
    }
}
StringifyImageDirective.ɵfac = function StringifyImageDirective_Factory(t) { return new (t || StringifyImageDirective)(i0.ɵɵdirectiveInject(i0.Renderer2), i0.ɵɵdirectiveInject(i0.ElementRef)); };
StringifyImageDirective.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: StringifyImageDirective, selectors: [["", "clickStringifyImage", ""]], hostBindings: function StringifyImageDirective_HostBindings(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵlistener("change", function StringifyImageDirective_change_HostBindingHandler($event) { return ctx.onChange($event.target.files); });
        }
    }, inputs: { model: "model" }, outputs: { modelChange: "modelChange" } });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(StringifyImageDirective, [{
            type: Directive,
            args: [{
                    selector: '[clickStringifyImage]'
                }]
        }], function () { return [{ type: i0.Renderer2 }, { type: i0.ElementRef }]; }, { model: [{
                type: Input
            }], modelChange: [{
                type: Output
            }], onChange: [{
                type: HostListener,
                args: ['change', ['$event.target.files']]
            }] });
})();

class MongoDatePipe {
    transform(value) {
        var _a, _b;
        if (value instanceof Date) {
            return value;
        }
        if ((_a = value === null || value === void 0 ? void 0 : value.$date) === null || _a === void 0 ? void 0 : _a.$numberLong) {
            // @ts-ignore
            return new Date(parseInt((_b = value === null || value === void 0 ? void 0 : value.$date) === null || _b === void 0 ? void 0 : _b.$numberLong));
        }
        return value;
    }
}
MongoDatePipe.ɵfac = function MongoDatePipe_Factory(t) { return new (t || MongoDatePipe)(); };
MongoDatePipe.ɵpipe = /*@__PURE__*/ i0.ɵɵdefinePipe({ name: "mongoDate", type: MongoDatePipe, pure: true });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(MongoDatePipe, [{
            type: Pipe,
            args: [{
                    name: 'mongoDate'
                }]
        }], null, null);
})();

class DragDropFilesDirective {
    constructor() {
        this.fileDropped = new EventEmitter();
        this.cssClass = 'click-drag-drop-files';
    }
    // Dragover Event
    dragOver(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-over';
    }
    // Dragleave Event
    dragLeave(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-leave';
    }
    // Drop Event
    drop(event) {
        event.preventDefault();
        event.stopPropagation();
        this.cssClass = 'click-drag-drop-files click-drag-drop-files-drop';
        const files = event.dataTransfer.files;
        if (files.length > 0) {
            this.fileDropped.emit(files);
        }
    }
}
DragDropFilesDirective.ɵfac = function DragDropFilesDirective_Factory(t) { return new (t || DragDropFilesDirective)(); };
DragDropFilesDirective.ɵdir = /*@__PURE__*/ i0.ɵɵdefineDirective({ type: DragDropFilesDirective, selectors: [["", "clickDragDropFiles", ""]], hostVars: 2, hostBindings: function DragDropFilesDirective_HostBindings(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵlistener("dragover", function DragDropFilesDirective_dragover_HostBindingHandler($event) { return ctx.dragOver($event); })("dragleave", function DragDropFilesDirective_dragleave_HostBindingHandler($event) { return ctx.dragLeave($event); })("drop", function DragDropFilesDirective_drop_HostBindingHandler($event) { return ctx.drop($event); });
        }
        if (rf & 2) {
            i0.ɵɵclassMap(ctx.cssClass);
        }
    }, outputs: { fileDropped: "fileDropped" } });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DragDropFilesDirective, [{
            type: Directive,
            args: [{
                    selector: '[clickDragDropFiles]'
                }]
        }], null, { fileDropped: [{
                type: Output
            }], cssClass: [{
                type: HostBinding,
                args: ['class']
            }], dragOver: [{
                type: HostListener,
                args: ['dragover', ['$event']]
            }], dragLeave: [{
                type: HostListener,
                args: ['dragleave', ['$event']]
            }], drop: [{
                type: HostListener,
                args: ['drop', ['$event']]
            }] });
})();

class ClickModule {
    // I setup the module providers for the root application.
    static forRoot(options) {
        return ({
            ngModule: ClickModule,
            providers: [
                // In order to translate the raw, optional OPTIONS argument into an
                // instance of the MyServiceOptions TYPE, we have to first provide it as
                // an injectable so that we can inject it into our FACTORY FUNCTION.
                {
                    provide: FOR_ROOT_OPTIONS_TOKEN,
                    useValue: options
                },
                // Once we've provided the OPTIONS as an injectable, we can use a FACTORY
                // FUNCTION to then take that raw configuration object and use it to
                // configure an instance of the MyServiceOptions TYPE (which will be
                // implicitly consumed by the MyService constructor).
                {
                    provide: ClickModuleOptions,
                    useFactory: provideApiServiceOptions,
                    deps: [FOR_ROOT_OPTIONS_TOKEN]
                }
                // NOTE: We don't have to explicitly provide the MyService class here
                // since it will automatically be picked-up using the "providedIn"
                // Injectable() meta-data.
            ]
        });
    }
}
ClickModule.ɵfac = function ClickModule_Factory(t) { return new (t || ClickModule)(); };
ClickModule.ɵmod = /*@__PURE__*/ i0.ɵɵdefineNgModule({ type: ClickModule });
ClickModule.ɵinj = /*@__PURE__*/ i0.ɵɵdefineInjector({ providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthorizationInterceptor,
            multi: true
        }
    ], imports: [CommonModule,
        HttpClientModule] });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ClickModule, [{
            type: NgModule,
            args: [{
                    declarations: [ApiErrorComponent, StringifyImageDirective, MongoDatePipe, DragDropFilesDirective],
                    imports: [
                        CommonModule,
                        HttpClientModule,
                    ],
                    providers: [
                        {
                            provide: HTTP_INTERCEPTORS,
                            useClass: AuthorizationInterceptor,
                            multi: true
                        }
                    ],
                    exports: [
                        ApiErrorComponent,
                        StringifyImageDirective,
                        MongoDatePipe,
                        DragDropFilesDirective
                    ]
                }]
        }], null, null);
})();
(function () {
    (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ClickModule, { declarations: [ApiErrorComponent, StringifyImageDirective, MongoDatePipe, DragDropFilesDirective], imports: [CommonModule,
            HttpClientModule], exports: [ApiErrorComponent,
            StringifyImageDirective,
            MongoDatePipe,
            DragDropFilesDirective] });
})();
// I am the token that makes the raw options available to the following factory function.
// --
// NOTE: This value has to be exported otherwise the AoT compiler won't see it.
const FOR_ROOT_OPTIONS_TOKEN = new InjectionToken('forRoot() ApiService configuration.');
// I translate the given raw OPTIONS into an instance of the MyServiceOptions TYPE. This
// will allows the MyService class to be instantiated and injected with a fully-formed
// configuration class instead of having to deal with the Inject() meta-data and a half-
// baked set of configuration options.
// --
// NOTE: This value has to be exported otherwise the AoT compiler won't see it.
function provideApiServiceOptions(options) {
    // If the optional options were provided via the .forRoot() static method, then apply
    // them to the MyServiceOptions Type provider.
    if (options) {
        if (typeof (options.apiUrl) === 'string') {
            ClickModuleOptions.apiUrl = options.apiUrl;
        }
        if (typeof (options.logging) !== 'undefined') {
            ClickModuleOptions.logging = options.logging;
        }
        if (typeof (options.loginRoute) !== 'undefined') {
            ClickModuleOptions.loginRoute = options.loginRoute;
        }
        if (typeof (options.apiSecretKey) !== 'undefined') {
            ClickModuleOptions.apiSecretKey = options.apiSecretKey;
        }
        ClickModuleOptions.lock();
    }
    return (ClickModuleOptions);
}

class AuthenticationService {
    /**
     * Constructor
     */
    constructor(apiService) {
        this.apiService = apiService;
        /**
         * The authenticated user data
         */
        this.authenticatedUser = null;
    }
    /**
     * Authenticates a user
     * Returns a promise so you can use await
     */
    authenticate(data, url = null, params = null, headers = null) {
        // always destroy the user before remaking it
        this.destroy();
        if (url === null) {
            url = 'admin/authenticate';
        }
        return new Promise((resolve, reject) => {
            let base = this.apiService.post(url, data, params, headers);
            if (base instanceof Observable) { // make sure we are always a promise
                base = base.toPromise();
            }
            base.then(response => {
                resolve(new AuthenticatedUserModel(response));
            }).catch(error => {
                reject(error);
            });
        });
    }
    /**
     * Destroys a user
     */
    destroy() {
        if (this.authenticatedUser === null) {
            this.authenticatedUser = new AuthenticatedUserModel();
        }
        this.authenticatedUser.destroyUser();
    }
}
AuthenticationService.ɵfac = function AuthenticationService_Factory(t) { return new (t || AuthenticationService)(i0.ɵɵinject(ApiService)); };
AuthenticationService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthenticationService, factory: AuthenticationService.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthenticationService, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: ApiService }]; }, null);
})();

class ComponentBridgeSubjectsModel {
    constructor(id, subject) {
        this.id = id;
        this.subject = subject;
    }
}

class ComponentBridgeService {
    constructor() {
        /**
         * An array that stores all the subjects
         */
        this.subjects = [];
    }
    /**
     * Sends a message
     */
    send(id, message) {
        this.getTransport(id).next(message);
    }
    /**
     * Subscribes to messages
     */
    subscribe(id) {
        return this.getTransport(id).asObservable();
    }
    /**
     * Destroys the subscription
     */
    unsubscribeAll(id) {
        for (let i = 0; i < this.subjects.length; i++) {
            if (this.subjects[i].id === id) {
                this.subjects.splice(i, 1);
                break;
            }
        }
    }
    /**
     * Gets the transport
     */
    getTransport(id) {
        for (let i = 0; i < this.subjects.length; i++) {
            if (this.subjects[i].id === id) {
                return this.subjects[i].subject;
            }
        }
        // if we get here, we dont have one, auto-create it
        const transport = new ComponentBridgeSubjectsModel(id, new Subject());
        this.subjects.push(transport);
        return transport.subject;
    }
}
ComponentBridgeService.ɵfac = function ComponentBridgeService_Factory(t) { return new (t || ComponentBridgeService)(); };
ComponentBridgeService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: ComponentBridgeService, factory: ComponentBridgeService.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(ComponentBridgeService, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], null, null);
})();

class GetIdentityResponseModel {
}

class AuthenticatedGuard {
    /**
     * Constructor
     */
    constructor(router) {
        this.router = router;
        this.user = new AuthenticatedUserModel();
    }
    /**
     * Can Activate
     */
    canActivate(next, state) {
        return this.guard();
    }
    /**
     * Can Activate Child
     */
    canActivateChild(next, state) {
        return this.guard();
    }
    /**
     * Can Load
     */
    canLoad(route, segments) {
        return this.guard();
    }
    /**
     * Tests if the user is valid and redirects if route is not null
     */
    guard() {
        if (!this.user.isValid()) {
            if (ClickModuleOptions.loginRoute !== null) {
                this.router.navigateByUrl(ClickModuleOptions.loginRoute);
            }
            return false;
        }
        return true;
    }
}
AuthenticatedGuard.ɵfac = function AuthenticatedGuard_Factory(t) { return new (t || AuthenticatedGuard)(i0.ɵɵinject(i1$1.Router)); };
AuthenticatedGuard.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AuthenticatedGuard, factory: AuthenticatedGuard.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AuthenticatedGuard, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: i1$1.Router }]; }, null);
})();

class UnsavedWorkGuard {
    canDeactivate(component, currentRoute, currentState, nextState) {
        if (!component.canDeactivate()) {
            if (confirm('You have unsaved changes! If you leave, your changes will be lost.')) {
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    }
}
UnsavedWorkGuard.ɵfac = function UnsavedWorkGuard_Factory(t) { return new (t || UnsavedWorkGuard)(); };
UnsavedWorkGuard.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: UnsavedWorkGuard, factory: UnsavedWorkGuard.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(UnsavedWorkGuard, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], null, null);
})();

class AbstractFormComponent {
    constructor() {
        /**
         * Submitted flag
         */
        this._submitted = false;
    }
    /**
     * Host listener for back button protection
     */
    unloadNotification($event) {
        if (!this.canDeactivate()) {
            $event.returnValue = true;
        }
    }
    get submitted() {
        return this._submitted;
    }
    /**
     * Whether the component can deactivate or not
     */
    canDeactivate() {
        return this._submitted || !this.form.dirty;
    }
}
AbstractFormComponent.ɵfac = function AbstractFormComponent_Factory(t) { return new (t || AbstractFormComponent)(); };
AbstractFormComponent.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: AbstractFormComponent, factory: AbstractFormComponent.ɵfac });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(AbstractFormComponent, [{
            type: Injectable
        }], null, { unloadNotification: [{
                type: HostListener,
                args: ['window:beforeunload', ['$event']]
            }] });
})();

class MongoIdModel {
}

class MongoDateModel {
}

/*
 * Public API Surface of click
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AbstractFormComponent, ApiErrorComponent, ApiRequestModel, ApiService, AuthenticatedGuard, AuthenticatedUserEvent, AuthenticatedUserModel, AuthenticationService, ClickModule, ClickModuleOptions, ComponentBridgeService, DragDropFilesDirective, FOR_ROOT_OPTIONS_TOKEN, GetIdentityResponseModel, Hash, MongoDateModel, MongoDatePipe, MongoIdModel, StringifyImageDirective, UnsavedWorkGuard, provideApiServiceOptions };
//# sourceMappingURL=click-common.mjs.map
