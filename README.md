# Click - Common Module #

## About ##

This module contains commonly used services, models and components:

* ApiService - for making api calls
* ApiErrors - component for displaying API validation errors
* AuthenticationService - for authenticating/deauthing users
* AuthenticatedUserModel - for working with OAuth data, user validity, i.e., ```model.isValid()```
* Authenticated Guard - for protecting routes by checking authentication validity
* Hash - A utility for generating unique hashes/ids


## Installation ##

```angular2
import {ClickModule, ClickModuleOptions} from '@click/common';
import {environment} from '../environments/environment';

ClickModuleOptions.setOptions({
  apiUrl: environment.apiUrl,
  apiSecretKey: environment.apiSecretKey});
  
@NgModule({
  imports: [
    ClickModule.forRoot(ClickModuleOptions),
  ],
})
```

## Module Options ##

### ApiService Options ###

* apiUrl: API Base URL: ```https://www.somesite.com/api/```.
* logging: When true, the service will log the request params (default: true)
* loginRoute: The route to redirect to when a guard fails (See Authenticated Guard)
* apiSecretKey: This is used to make sure that authentication credentials don't clash. Each project should have its own key

You pass options to the ```forRoot()``` function of the module. Not all options need to be present. A typical example might look like:

```angular2  
ClickModuleOptions.setOptions({
  apiUrl: environment.apiUrl,
  apiSecretKey: environment.apiSecretKey});
  
ClickModule.forRoot(ClickModuleOptions),
// we use the environment variable so we can change urls based on build
```
## Api Service ##

The API service is a convenience wrapper for the HTTP Client that will inject the necessary headers based on the user authentication. 

Note that the ```Authorization``` header will only be injected if we are calling the domain name set forth in the ```apiUrl``` property of the module options and using the ```ApiService``` service. If you need to call another domain, such as Twitter, you should use Angular's ```HttpClient```. 

#### Usage ####

```angular2
import {ApiService} from '@click/common';

// in some function:

// GET
this.api.get('someUrl').then(response => {
    // do something with the reponse
}).catch(error => {
    // do something with the error
}).finally(() => {
    // do something finally
});

// POST
this.api.post('someUrl', {someKey: someValue});

// PUT
this.api.put('someUrl', {someKey: someValue});

// PATCH
this.api.patch('someUrl', {someKey: someValue});

// DELETE
this.api.delete('someUrl');

// You can also do a direct "call": 
this.api.call('someUrl', ApiService.PUT, {someKey: someValue}, new HttpParams({param: 'test'}));

```

#### Using await and async ####

Consider the following function that would make two API calls based on the results of the first: 

```angular2
getSomeDate() {
    this.loading = true;
    this.apiService.get('url').then(result => {
       this.apiService.get('something/else/'+result.id).then(result2 => {
            this.whatIwanted = result2;
        }).catch(error2 => {
            console.error(error2);
        }).finally(() => {
            this.loading = false;
        });   
    }).catch(error => {
        this.loading = false;
        console.log(error);
    });
}
```

By using TypeScripts ```async/await``` keywords, we can make this much cleaner:

```angular2
async getSomeData() {
    try {
       this.loading = true;
       const result = await this.apiService.get('url');
       const whatIwanted = await this.apiService.get('something/else/'+result.id);
       this.whatIwanted = whatIwanted;     
    } catch(e) {
       console.log(error);
    }

    this.loading = false;   
}
```

No longer do we have nest multiple ```then()```, ```catch()``` and ```finally()``` wrappers. We simply ```await``` the response from the first call before moving to the next call.

## API Errors ##

Instead of having to manually loop through and parse API errors that are returned from our restfull APIs, we can use the Api Errors Component: 

```angular2
<click-api-errors [apiErrors]="apiErrors" key="name" prependAsterisks="true"></click-api-errors>
``` 

Where apiErrors is an instance of ```HttpErrorResponse``` which is what is returned from the ```ApiService``` upon error and the ```key``` property is the name of the field this instance applies to. 

```prependAsterisks``` flag is a boolean that when ```true```, will prepend error messages with an asterisks.

The styles, of course, can be overridden. The raw markup of the components template is as follows: 

```angular2html
<ul *ngIf="errors.length" class="click-api-errors">
  <li *ngFor="let error of errors">{{error}}</li>
</ul>
```

The raw default SCSS for the component is as follows: 

```scss
:host {
  ul {
    margin: 0;
    padding: 0;
    list-style: none;
    display: block;
    li {
      color: darkred;
      font-size:.9rem;
      display: block;
    }
  }
}
```

## Authentication Service ##

The Authentication service manages user state and facilitates the login/logout

#### Authenticating/Logout ####

```angular2
import {AuthenticationService, AuthenticatedUserModel} from '@click/common';

// inject it where we need it: 
constructor(private authenticationService: AuthenticationService) {}

// Do a login
async login() {
    // authentication data from a form or model
    try {
        const user = await this.authenticationService.authenticate(someDataFromSomewhere, '/someurl')
        this.user = user;
    } catch(e) {
        // login failed
    }
}

// Do a logout
logout() {
    this.authenticationService.destroy();
}
```

If you do not specify a URL, the AuthenticationService will assume it is ```admin/authenticate```

#### Testing for User State ####

```angular2
// in some function
const user = new AuthenticatedUserModel();

if (user.isValid()) {
    // user is logged in
} else {
    // user is not logged in
}
```


#### Listening for changes in User State ####

You can subscribe to events that can occur that affect user state, ```AUTHENTICATED``` and ```DEAUTHENTICATED```

```angular2
// in some function, usually the constructor
const user = new AuthenticatedUserModel();
user.onChanges.subscribe((event: AuthenticatedUserEvent) => {
  // the user state has changed
  // event.type will be either AUTHENTICATED or DEAUTHENTICATED
  // event.model will be an instance of AuthenticatedUserModel
  
  console.log('User State Changed to:', event.type, 'And Validity is', event.model.isValid());
  
  if (event.type === AuthenticatedUserEvent.DEAUTHENTICATED) {
    // we are no longer authenticated. Lets close our menus and go to login screen
    this.closeMenus();
    this.router.navigateByUrl('/login');
  }
});
```


#### Getting A Users Record Using AuthenticatedUserModel and ApiService ####

```angular2
async getUser() {
    try {
        const authedUser = new AuthenticatedUserModel();
        if (authedUser.isValid()) {
            // user is logged in
            const user = await this.api.get(`url-to-user-api/${user.id}`);
            this.user = user;
        } else {
            // user is not logged in
            throw new Error('Cannot get user because we are not logged in!');
        }
    } catch(e) {
        console.error(e);
    }
}
```

If the API is using the API Authentication module, you can get the current users record by doing the following: 

```angular2
async getUser() {
    try {
      const user: GetIdentityResponseModel = await this.api.get('get-identity');
      
      // the repsonse from the get-identity API endpoint returns an object with two keys:
      // user: any - This is the users actual database record
      // collection: string - This is the mongo collection that the user was retrieved from
      
      this.user = user.user;
    } catch (e) {
      // we are not logged in
      console.error(e);
    }
}
```


#### Reusing the User object ####

You do not need to create a new object every time you wish to check for user validity or to get user properties. If you have a component that has a lot of interactions with the oauth user data or testing for validity consider instantiating the object once in the constructor:

```angular2
export class AppComponent {
    public authedUser: AuthenticatedUserModel;

    constructor() {
        this.authedUser = new AuthenticatedUserModel();  
    }
    
    someFunction() {
        if (this.authedUser.isValid()) {
            // do something
        }
    }
}
```

## Authenticated Guard ##

This module has a prebuilt guard that can be used to protect routes. The guard implements the ```canActivate```, ```canActivateChild``` and ```canActivateChild``` methods.

If the user is not logged in and a protected route is invoked, the guard will return false, blocking the components execution and redirecting the user to the ```/login``` route. The login route can be changed by changing the ```loginRoute``` property of the module options. If the ```loginRoute``` is set to ```null```, no redirect will occur. 

Adding the guard to the route depends on the type of Component/Module you are trying to route to. See the Angular documentation for more information. Typically, a protected route might look like:

```angular2
import {AuthenticatedGuard} from '@click/common';

const routes: Routes = [
  { path: 'someModule', loadChildren: './some-module/some-module.module#SomeModule', canLoad: [AuthenticatedGuard] },
  { path: 'someUrl', component: SomeComponent, canActivate: [AuthenticatedGuard] }
]
```


## Hash ##

Quite often, we need to generate unique IDs for new elements. We can use the ```Hash``` utility to generate guaranteed unique ID hashes:

```angular2
// import the utility
import {Hash} from '@click/common';

// in some function
Hash.get(); // Output similar to 5d4b48486c49bc853ce88cb15938d1
Hash.get(); // Output similar to 5d4b49da50624c1ce6748c278cc082
Hash.get(); // Output similar to 5d4b49dfb0e560bc9f507a641a20b
```


## Component Bridge ##

Quite often, we need to communicate with other components or between unrelated components. We can do this with events or we can use the ComponentBridgeService:

```typescript

// In Component A

import {ComponentBridgeService} from '@click/common';

export class ComponentA {

    constructor(private componentBridge: ComponentBridgeService) {}
   
    sendMessage() {
        this.componentBridge.send('some-id', 'This is a message from App Component');
    }
    
    sendDifferentMessage() {
        this.componentBridge.send('some-other-id', 'This is a message from App Component');
    }

}
```

```typescript
// In Component B

import {ComponentBridgeService} from '@click/common';

export class ReceiverComponent implements OnInit, OnDestroy {

    private componentASubscription: Subscription;
    private componentADifferentSubscription: Subscription;

    constructor(private componentBridge: ComponentBridgeService) {}
    
    ngOnInit() {
      this.componentASubscription = this.componentBridge.subscribe('some-id').subscribe((message) => {
        // do something with the message we received from Component A
      });
      
      this.componentADifferentSubscription = this.componentBridge.subscribe('some-other-id').subscribe((message) => {
        // do something different with the different message we received from Component A
      });
    }

    ngOnDestroy(): void {
      // we should always clean up subscriptions we make
      this.componentASubscription.unsubscribe();
      this.componentADifferentSubscription.unsubscribe();
    }
}
```

If we need to completely destroy a bridge, we can do so by using the ```unsubscribeAll()``` method. NOTE: this will not destroy your local subscriptions and they should be cleaned up manually as you see fit, either at the same time or on destroy:

```typescript
ngOnDestroy(): void {
  this.componentBridge.unsubscribeAll('some-id');

  // we should always clean up subscriptions we make
  this.componentASubscription.unsubscribe();
  this.componentADifferentSubscription.unsubscribe();
}

``` 


## Stringify Image ##

Quite often, we need to upload images, but we need to preview them first. In these cases, we stringify or base64 encode them and send that string to the API. The ```clickStringifyImage``` directive facilitates that functionality. 

To use it, add the ```clickStringifyImage``` to a file input. To get the data via two-way binding, use the ```model``` property. The model can either be a traditional object, or a ```FormControl``` object that supports validation.

This directive also supports the ```multiple``` property and will return an array of stringified images in that case. 

```angular2html

<h1>Test Image (Traditional Model)</h1>
<input type="file" name="test" id="test"  accept=".png,.jpg,.gif" clickStringifyImage [(model)]="imageModel.image"/>
<img [src]="imageModel.image"/>

<h1>Test Multiple Image (Traditional Model)</h1>
<input type="file" name="test2" id="test2"  accept=".png,.jpg,.gif" clickStringifyImage [(model)]="imageModel.multiple" multiple/>
<img [src]="image" *ngFor="let image of imageModel.multiple"/>

<h1>Test Image (Form Control)</h1>
<input type="file" name="test3" id="test3"  accept=".png,.jpg,.gif" clickStringifyImage [(model)]="formControl1"/>
<img [src]="formControl1.value"/>
<p>Valid: {{formControl1.valid | json}}</p>

<h1>Test Multiple Image (Form Control)</h1>
<input type="file" name="test4" id="test4"  accept=".png,.jpg,.gif" clickStringifyImage [(model)]="formControl2" multiple/>
<img [src]="image" *ngFor="let image of formControl2.value as Array"/>
<p>Valid: {{formControl2.valid | json}}</p>
```

## Mongo Date ##

This module provides a model for Mongo date object and a filter that will filter Mongo date objects to a native Javascript Date object.

```typescript
import {MongoDateModel} from "./mongo-date.model";

public dateTimeCreated: MongoDateModel;

// In a template:
Date: {{dateTimeCreated | mongoDate | date: 'short'}}

```
## Drag And Drop Files ##

This module provides a directive to support drag and drop files. The div with the directive will get the base class ```click-drag-drop-files```. On certain events the div will be augmented with additional classes:

* onDragOver: ```click-drag-drop-files-over```
* onDragLeave: ```click-drag-drop-files-leave```
* onDragDrop: ```click-drag-drop-files-drop```

```angular2html
<div clickDragDropFiles (fileDropped)="receiveFiles($event)">
  <h5>Drag And Drop Here</h5>
</div>
```


```typescript
// A receiver function in a component
receiveFiles(files: FileList) {
    // Do something with the files
}
```

## Abstract Form Component and Unsaved Work Guard ##

The abstract form component is used to facilitate warnings before people leave a form before submitting their changes. Any component that you want to have this ability for should extend this class. 

Additionally, you will need to configure the router to be aware of this feature on a case by case basis.

```typescript
// Your component:
export class MyFormComponent extends AbstractFormComponent {

  constructor() {
    super();
  }
}

// The routing config for this component:
const routes: Routes = [
  {path: ':id', component: MyFormComponent, canActivate: [AdminGuard], canDeactivate: [UnsavedWorkGuard]},
];
```

## Who do I talk to? ##

Derek Miranda (derek@zdidesign.com)
